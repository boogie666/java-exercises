const Markdown = require("markdown-to-html").Markdown;
const Mustache = require("mustache");
const stream = require('stream');
const util = require('util');
const Transform = stream.Transform;

function MustacheStream(opt){
  Transform.call(this, opt);
  this.buffer = "";
  this.articleTemplate = opt.template;
  this.articleTitle = opt.title;
}

util.inherits(MustacheStream, Transform);

MustacheStream.prototype._transform = function(chunk, enc, cb){
  this.buffer += chunk.toString("UTF-8");
  cb();
};

MustacheStream.prototype._flush = function(cb){
  this.push(Mustache.render(this.articleTemplate, { title: this.articleTitle, content: this.buffer}));
  cb();
};


const fs = require("fs");

function articleStream(data){
  return new MustacheStream({ title: data.title, template: fs.readFileSync("./templates/article.html", "UTF-8") });
}

function markdownStream(filePath, templateStream){
  let md = new Markdown();
  md.render(filePath, {}, function(){
    md.pipe(templateStream);
  });
}

function renderIndex(data){
  let tpl = fs.readFileSync("./templates/index.html", "UTF-8");
  return Mustache.render(tpl, data);
}

let categories = fs.readdirSync("../exercises");
let exercises = categories.map(function(cat){
  return { title: cat, items: fs.readdirSync("../exercises/" + cat).map(function(title){
    let name = title.replace(".md", "");
    let filePath = cat.toLowerCase() + "/" + title.toLowerCase().split(" ").join("_").replace(".md","") + ".html";
    let linkPath = encodeURIComponent(cat.toLowerCase()) + "/" + title.toLowerCase().split(" ").join("_").replace(".md","") + ".html";
    return { 
      link: "/" + linkPath, 
      name: name,
      htmlPath: "../public/" + filePath,
      filePath: "../exercises/" + cat + "/" + title
    };
  })};
});


fs.writeFileSync("../public/index.html", renderIndex({title: "Exercitii Java", categories: exercises}));

exercises.forEach(function(category){
   if(!fs.existsSync("../public/" + category.title.toLowerCase()))
     fs.mkdirSync("../public/" + category.title.toLowerCase());
   category.items.forEach(function(item){
      let as = articleStream({ title: item.name });
      markdownStream(item.filePath, as);
      let ws = fs.createWriteStream(item.htmlPath);
      as.pipe(ws);
   });
});


//let as = articleStream({ title: "Hello World" });
//parseArticle("./exercises/Bucle/Factorial.md", as);
//as.pipe(process.stdout);


