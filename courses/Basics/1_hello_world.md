#  Hello World

Programul de tip `Hello World` este, deobicei, primul program
scris de oricine cand invata un limbaj nou.

Aste program este considerat `cel mai simplu program` care si face ceva...

Executia lui consta in afisarea mesajului `Hello World` in consola.

## Java 

Intr-ul fisier numit `Hello.java` se pune textul urmator:

```
public class Hello{
  public static void main(String[] args){
    System.out.println("Hello World");
  }
}
```


## Compilare

Inainte sa rulam programul, trebuie sa il compilam.
Prin compilare, transformam `codul sursa` intr-un program.


In `Command Prompt`, fiind in folder-ul cu fisierul `Hello.java`
rulam compilatorul 
```
  javac Hello.java
```

Asta ne va produce un nou fisier numit `Hello.class`


## Rulare

Dupa compilare, putem rula programul `Hello.class` astfel:

```
  java Hello
```

asta va afisa in consola mesajul 

```
Hello World
```

dupa care programul is va termina executia.


## IDE-uri

Este foarte incomod sa scriem si sa rulam cod in felul acesta.
Pentru a fi mai productivi putem folosi un editor de cod.

Exista mai multe feluri de editoare de cod.

Editoare simple si IDE-uri.

Un editor simplu, precum Notepad++, Atom, VisualStudioCode, Vim etc. 
Ne ofera un numar limitat de functionalitati, deobicei doar colorarea sintaxei.
Dar pentru anumite limbaje este suficient.

Pentru `java` putem folosii un IDE.

Un IDE (sau Integrated Development Enviroment) ne ofera mult mai mult decat sintaxa colorata.
Putem sa rulam programul direct din IDE, sa depanam (debug) program, sa navigam mai usor prin cod 
si multe alte functionalitati.

La curs, v-om folosi `Eclipse`.


## Eclipse

In eclipse compilarea se face automat, in timp ce scriem,
iar rularea se poate face  apasand pe buttonul verde (numit `Run`)

(mai multe detalii la curs)



