# Conditionale

In programare este, foarte des, necesar sa luam decizii, in ceea ce priveste executia codului.

Aceste decizii se pot face, in cod, cu ajutorul lui `if`.

## Sintaxa

Un `if` in java se scrie in felul  urmator:

```
  if (true) {
    System.out.println("Hello World");
  }
```

Observam sintaxa urmatoarea:

cuvantul cheie `if` este urmat de `(`  si `)`
intre paranteze este obligatoriu sa avem o expresie care rezulta intr-o valoare de tip `boolean`.
(adica `true` sau `false`)

Daca valoarea expresiei este `true`, programul va executa codul dintre paranteze.
Daca valoarea este `false`, programul nu va executa codul dintre paranteze.

In cazul de mai sus, programul va afisa mesaul `Hello World`.


### Mai complex un pic.

```
  int x = 10;
  if( x < 5 ){
    System.out.println("Hello World");
  }
```

Observam ca, acest program verifica daca `x` este mai mic ca `5`.
Si daca este, va afisa mesajul `Hello World`, altfel nu.

Cum valoarea lui `x` este `10`, iar `10` nu este mai mic decat `5`,
acest program nu va face nimic.

Exercitiu:

  Modificati, in 3 moduri diferite, programul astfel in cat el sa afiseze mesajul `Hello World`.



### "else"

Putem extinde exemplu de mai sus astfel in cat sa afisam un mesaj
cand `x` este mai mic ca `5` si ALT mesaj in caz contrar.

```
  int x = 10;
  if(x < 5){
    System.out.println("Hello World");
  } else {
    System.out.println("Nu...");
  }
```

In cazul acesta, programul va afisa mesajul `Hello World` 
daca `x` este mai mic ca `5`

altfel va afisa mesajul `Nu...`...

Cum `x`, din nou nu este mai mic ca `5`, programul va afisa mesajul `Nu...`.

### "else if"

Putem inlantuii mai multe conditii folosind `else if` in felul urmator.

```
  int x = 10;
  if(x < 5){
    System.out.println("Hello World");
  } else if (x < 7) {
    System.out.println("Goodbye World");
  }
```

Acum programul va afisa `Hello World` daca x este mai mic ca 5,
`Goodbye World` daca x este mai mic ca 7 si nu va face nimic in caz contrar.

Mai putem inlantuii cate `else if`-uri dorim.


## Operatori "conditionali"

Putem definii ca operatori conditionali, acei operatori a caror rezultat este `true` sau `false`.
Altfel spus, un operator conditional reprezinta o intrebare a carui raspuns este `DA` sau `NU`.

In ceea ce priveste numerele, in java, putem pune urmatoarele intrabari (cu raspuns `DA` sau `NU`):
   * `x == y`  - sau este `x` egal cu (sau identic cu) `y`
   * `x < y`   - sau este `x` mai mic ca `y`
   * `x > y`   - sau este `x` mai mare ca `y`
   * `x <= y`  - sau este `x` mai mic sau egal ca `y`
   * `x >= y`  - sau este `x` mai mare sau egal ca `y`
   * `x != y`  - sau este `x` diferit `y`

## Operatori logici

Definim ca operator logic, acei operatori care opereaza cu valori de tip `boolean`.
De exemplu:
   ` x > 5 && x < 7 ` se citeste `x mai mare ca 5 SI x mai mic ca 7`
   ` x < 5 || x > 7 ` se citeste `x mai mic ca 5 SAU x mai mare ca 7`
   ` !(x < 5) ` se citeste `NU x mai mic ca 5` (sau `x NU este mai mic ca 5`)

Altfel spus, un operator logic reprezinta o expresie conditionala mai complexa, ca carui raspuns depinde de mai multe `DA`-uri si `NU`-uri.

Operatorii sunt:
    * `a && b` - sau `a SI b`, care este adevar doar daca `a` este `true` si `b` este `true`, altfel este `false`
    * `a || b` - sau `a SAU b`, care este adevar doar daca cel putin una din parti este `true`, altfel este `false`.
    * `!a` - sau `NU a`, care inverseaza valoare de adevar a lui `a`, daca `a` este `true`, `NU a` va fi `false`, si invers.


## Tema

1. Scrieti un program care va afisa urmatoarele mesaje, cand se indeplinesc conditiile aferente mesajului.
    * Daca `x` este mai mic ca `5` afisati `x este mai mic ca 5`
    * Altfel daca `x` este mai mic ca `10` afisati `x este mai mic ca 10`
    * Altfel daca `x` este mai mic ca `15` afisati `x este mai mic ca 15`
    * Altfel afisati mesajul `Nu stiu ce ii cu 'x' asta`

2. Scrieti un program care face urmatoarele: 
    * Cand `x` este mai mic ca `20` afiseaza `X < 20`
    * Cand `x` este mai mic ca `15` afiseaza `X < 15`
    * Cand `x` este mai mic ca `5` afiseaza `X < 5`
    * Altfel afiseaza `X ii foare mic`
    * !!! Atentie la ordinea conditiilor !!!

3. Scrieti un program care afiseaza urmatoarele mesaje:
    * Cand `x` se afla intre `5` si `7`, afiseaza `In intervalul (5, 7)`
    * Cand `x` se afla intre `7 si 9`, afiseaza `In intervalul (7, 9)`
    * Altfel afiseaza `x nu se afla nici intervalul (5, 7), nici in (7, 9)`
