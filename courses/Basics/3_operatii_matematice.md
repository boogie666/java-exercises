# Operatii Matematice

In java, putem face calcule matematice folosind unul din operatorii de aritmetica
cu ar fi `+`, `-`, `*`, `/`, `%` etc...

De exemplu:

```
  int trei = 1 + 2;
```

Daca afisam in consola valoarea variabilei `trei`, 
vedem ca suma numerelor `1` si `2` este (dupa cum ne putem astepta) `3`...


```
public class Hello{
  public static void main(String[] args){
    int trei = 1 + 2;
    System.out.println(trei);
  }
}
```

Operatorii aritmetici sunt:
 * `+` - adunare
 * `-` - scadere
 * `*` - inmultire
 * `/` - impartire
 * `%` - modulo (adica restul impartii)

Toti operatorii se executa in ordinea de finita in matematica
(adica prima data inmultirea, urmat de impartire,urmat de modulo, urmat de adunare, urmat de scadere)

Adica, in exemplu urmator, `x` va avea valoarea  `7`, pentru ca `2 * 3` se evaluaza prima data, iar la rezultat (`6`) se aduna `1`.
La fel si pentru `y`.


```
  int x = 2 * 3 + 1; 
  int y = 1 + 2 * 3; 
```

Pentru a definii operatiile in alta ordine, folosim paranteze.

```
  int x = 2 * (3 + 1);
  int y = (1 + 2) * 3;
```

In cazul acesta, prima data se evalueaza parantezele prima data.
`x` va avea valoare `8`, pentru ca `3 + 1` este `4`, si `4 * 2` este `8`.
`y` va avea valoare `9`, pentru ca `1 + 2` este `3`, si `3 * 3` este `9`.

## Ierarhia numerelor.

In java, rezultatul unei operatie va avea tot tipul celui mai "puternic" operand,
in ordinea urmatoare (de la slab la puternic)

  1. byte
  2. short
  3. int
  4. long
  5. float 
  6. double


Asta inseamna ca, daca adunam un numar intreg cu un numar cu zecimale, rezultatul va fi cu zecimale.
(adica ` 1 + 2.5f ` este ` 3.5f `,  ` 1 + 2.5 ` este `3.5` double)

## Supraincarcare de operatori.

Supraincarcarea este posibilatea de a redefinii operatori matematice dupa bunul plac,
pe alte tipuri de date, in afara de cele numerice.

Multe limbaje ofera posibilitatea de a supraincarca operatorii matematici.
Java nu ofera aceasta posibilate.

Singurul operator care este supraincarcat in java, este operatorul `+` cu privire la `String`

Adica, `+` intre doua string-uri reprezinta concatenarea de siruri de caractere.
De exemplu:
```
  String nume = "Ion" + "Popescu";
```

Dupa aceasta operatie valoarea variabilei `nume`  este sirul de caractere `"IonPopescu"`.

String-ul este un este mai puternic decat result tipurilor primtive.
(adica `orice` + un `string` sau un `string` + `orice` va rezulta intr-un string)


## Tema

Scrieti un program care implementeaza regula de 3 simpla si afisati rezultatul.
Formula regului de trei simpla este urmatoarea:
```
  x  din  10
     este
  5  din  100

adica:
  x = (10 inmultit cu 5) totul imparti la 100
  
```


Pseudocod:

```
  fie a: double = 10;
  fie b: double = 5;
  fie c: double = 100;
  
  fie x: double = ( a * b ) / c;
  
  afiseaza x;
```
