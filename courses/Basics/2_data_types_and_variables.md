# Tipuri de date

In `java` avem posibilitatea sa folosim mai multe feluri de date pentru a scrie programe.
Tipurile de date se impart in doua categorii.

  1) Tipuri de date `primitive`
  2) Tipuri de date `compozite` (sau `compuse`)


## Primitive

Avem la dispozitie urmatoarele tipuri de date primitive.
  Numere:
    * `byte` - care reprezinta un numar intreg cu valori intre -128 si 127 
    * `short` - care reprezinta un numar intreg cu valori intre -32,768 si 32,767
    * `int` - care reprezinta un numar intreg cu valori intre -2,147,483,648  si 2,147,483,647 
    * `long` - care reprezinta un numar intreg cu valori intre -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
    * `float` - care reprezinta un numar zecimal cu precizie simpla
    * `double` - care reprezinta un numar zecimal cu precizie dubla
  
  Non-Numere:
    * `boolean` - care reprezinta valorile `true` si `false`
    * `char` - care reprezeinta o litra
    * `String` - care reprezinta un sir de caractere

`String`-ul nu este un primitiv, in adevaratul sens al cuvantului, dar poate fi considerat ca un primitiv.


Observam ca plaja valorilor pentru numere intregi pare sa fie destul de aleatoare.

De ar avea un byte valori intre -128 si 127?

## Reprezentare Binara

Dupa cum stim, calculatorul, la un nivel fundamental, stie doar de doua valori...
de `1` si `0`.
Aceste doua valori reprezinta, practic, daca un circuit este inchis (`0`) sau deschis (`1`)
Aceste doua valori se numesc un `bit`.

Dar daca grupam mai multi `biti` putem reprezenta mai multa informatie.

De exemplu:
  
  Putem reprezenta valorile `true` si `false` avem nevoie de doar 1 bit:
    1 -> true
    0 -> false

  Pentru a reprezenta valori intre 0 si 3 (adica 4 valori diferite)  am avea nevoie de 2 bits:
    00 -> cifra 0
    01 -> cifra 1
    10 -> cifra 2
    11 -> cifra 3

  Pentru a reprezenta valori intre 0 si 7 (adica 8 valori diferite) am avea nevoie de 3 bits:

    000 -> cifra 0
    001 -> cifra 1
    010 -> cifra 2
    011 -> cifra 3
    100 -> cifra 4
    101 -> cifra 5
    110 -> cifra 6
    111 -> cifra 7


Observam ca totul creste cu cate putere a lui 2.

* Pentru 2 valori avem nevoide de 2^1 bits.
* Pentru 4 valori avem nevoide de 2^2 bits.
* Pentru 8 valori avem nevoide de 2^3 bits.
* Pentru 16 valori avem nevoide de 2^4 bits.

Si pentru a reprezenta 256 de valori (adica un byte) avem nevoie de 2^8 bits.

## Numere

Numerele intregi se scrie simplu,

De exemplu, numarul `42` se scrie `42`.

Separatorul de zecimale este `.`, adica, pentru numarul `2,5` (doi si jumatate), scriem `2.5`.

In mod implicit, numarele cu zecimale sunt de tip `double`, iar numerele intregi sunt `int`.

Daca dorim sa scriem un numar de tip `float` adaugam un `f` la final, adica `2.5f`.
Iar daca dorim sa scriem un numar de tip `long`, adaugam un `l` sau `L` la final, adica `42l`/`42L`

## Booleans 

O valoare boolean poate aveam doar una din doua valori.

`true` sau `false`, scrie extract asa.

## Caractere

Caracterele in java sunt inconjurate de o ghilimea simpla (`'`)

Adica litera `a` se scrie `'a'`
Adica litera `b` se scrie `'b'`
Symbol `.` se scrie `'.'`.

Cifra `1`, reprezantata ca si caracter se scrie `'1'` (daca vrem numarul `1`, il scriem fara ghilimele)

## String-ul


Un String se scrie inconjurat de ghilimele duble (`"`)
Adica cuvantul `Bogdan`, se scrie `"Bogdan"`.

Mesajul `Hello world` se scrie `"Hello world"`.

String poate fi considerat primitive, dar totusi nu este.

In Java, la fel ca multe alte limbaje de programare, avem notiunea de 
sir de caractere.

In alte limbaje din familia `C`, sirul de caractere este definit
ca un simplu vector (sau lista) de caractere.

In Java, acest vector de caractere este `incapsulat` in tipul de date numit `String`.

Acesta incapsupare este facuta pentru a oferii o serie de funtionalitati in plus, fata de 
o simpla lista de litere, cum ar fi posibiltatea de a cauta intr-un sir de caractere o secventa.
sau de a extrage sub-secvente (SubStrings) din o secventa data.

Vom face mai multe despre String in capitolele mai avansate ale cursului.



# Variabile

Pentru a decara o variabila in `java` prima data trebuie sa ii specificam tipul de date, urmat de un nume al variabilei.

De exemplu, pentru a declara o variabila de tip intreg, numita `x` cu valoarea `1`, scriem urmatorul cod:

```
int x = 42;
```

Pentru un sir de caractere numit `message` cu valoarea `Hello World`, scriem astfel:

```
String message = "Hello World";
```

Dupa declarare putem folosii variabila in locul in care am folosi valoarea ei.

Codul urmator este echivalentul programului `Hello World`

```
public class Hello{
  public static void main(String[] args){
    String message = "Hello World";
    System.out.println(message);
  }
}

```

Putem observam ca, la rulare, acest program este exact echivalent cu programul original `Hello World`,
dar difera in structura.

Prima linie a programului, defineste o variabila de tip `String` cu numele `message` si cu valoarea `"Hello World"`.
Iar a doua linie, afiseaza valoarea variabilei `message` in consola.


