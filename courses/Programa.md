# Basics (6 topics)
  * Setup + a bit of history about programming
  * Hello world
  * Variables + primitive data types
  * Operatii Matematice
  * If statement
  * Arrays + for statement

# Functions (3 topics)
  * syntax + signiture
  * abstractization
  * recursion
  
# Classes (6 topics)
  * Syntax + declaration
  * Properties
  * Methods
  * static members
  * inheritance
  * interfaces

# ASCII Art Shapes (3 topics) 
  * Basics of software architecture
  * Object Composition
  * Basic Geometric Shapes 

# Data Structures (5 topics)
  * Stack
  * Queue
  * List
  * Map
  * Trees (basics)

# Algorithms (3 topics)
  * Big O
  * insertion sort
  * quicksort

# Design Patterns (3 topics)
  * Singleton
  * Iterator
  * Dependency Injection

# Poor man's photoshop (intro to image processing) (3 topics)
  * The Pixels library
  * Grayscale filter
  * Basic Layers

# Awesome space action (basics of game developemnt) (3 topics)
  * Intro to libgdx
  * Moving sprites around
  * User input
