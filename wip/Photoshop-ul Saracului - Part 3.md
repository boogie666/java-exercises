# Photoshop-ul Saracului - Part 3
## Blending

Descrirea proiectului mentioneaza notiunea de `compozitare`
iar diagrama UML specifica o notinue abstracta de `BlendMode`


Idea de compozitare nu inseamna altceva decat suprapunea a doua sa mai multe layer
unu peste celalant si modul in care se suprapun pixelii din layerle respective...

Fara a face nimic putem deja sa suprapunem doua, sau mai multe layerle...

In Main putem sa facem urmatoarele:

```
   Layer l = new ImageLayer(0, 0, Image.load("o poza.png"));
   Layer l2 = new ImageLayer(200, 200, Image.load("o alta poza.png"));  

   Image i = new Image(1000, 1000);

   l.apply(i);
   l2.apply(i);

   i.write("poza_mare.png");

```

Observam ca trebuie sa aplicam ambele layere (`l` si `l2`) pe imaginea `i`


Daca ne uitam la poza generata vedem ambele poze suprapuse,
a doua fiind decalata cu 200 de pixeli pe x si y fata de prima...



