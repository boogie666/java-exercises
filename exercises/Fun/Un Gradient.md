# Un Gradient alb-negru

Folosind biblioteca "pixels-1.0.0.jar" generati un gradient pe
intreaga suprafata a unei imagine de 255x255;

### Pseudocod
``` 

  Image img = creaza un nou Image cu 255 si 255;

  pentru fiecare i de la 0 la img.getWidth():
    pentru fiecare j de la 0 la img.getHeight():
      img.putPixel(i, j, new Color(j, j, j)); // pentru un gradient de sus in jos
      img.putPixel(i, j, new Color(i, i, i)); // pentru un gradient de la stanga la dreapta, doar unu o sa functioneze, pick one...


  img.write("poza.png");  
     
```
