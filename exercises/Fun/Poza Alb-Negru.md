# Poza Alb-Negru

Folosind biblioteca "pixels-1.0.0.jar". 

Incarcati o poza, convertiti-o in alb/negru si salvati-o.

Metoda statica `Image.load("calea catre o poza");` va incarca poza in memorie.
Metoda `.write("calea unde vrem sa salvam poza");` va scrie poza pe disk.

Pentru a face o poza din color in alb-negru, fiecare canal de culoare trebuie setat
pe media celor 3 canale de culoare (R,G is B), pentru fiecare pixel.

#### Observatii

O metoda static poate apela doar alte metode statice...
atentie la metoda `albNegru`


### Pseudocod
``` 

  Image img = Image.load("picture.png");
  Image bwImage = new Image(img.getWidth(), img.getHeight()); 

  pentru fiecare i de la 0 la img.getWidth():
    pentru fiecare j de la 0 la img.getHeight():
      bwImage.putPixel(i, j, albNegru(img.getPixel(i, j)));


  

  fie albNegru(culoare) o functie care primeste o culoare ca parametru si returneaza o culoare noua:
    media = (culoare.getRed() + culoare.getGreen() + culoare.getBlue()) / 3;
    returneaza un Color nou din (media, media, media);

    
```
