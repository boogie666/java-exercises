# Forme si poze

Folosind biblioteca "pixels-1.0.0.jar" definitia de forme geometrice,
creata anterior desenati ceva...


#### Observatii.

Anterior am facut tot ce era nevoie pentru a creea forme geometrice.
Clasa `Scena` era responsabila pentru a 'desena' in consola.
Putem creea o nou clasa `Poza` similara cu scena in concept, 
dar care in loc sa printeze in terminal stie sa 'printeze' in poza.



### Pseudocod
``` 
  clasa Poza(Image img, Forma f):
    deseneaza():
      pentru fiecare i de la 0 la img.getWidth():
        pentru fiecare j de la 0 la img.getHeight():
          daca f.continePunct(i, j):
            img.putPixel(Color.BLACK);
          altfel:
            img.putPixel(Color.WHITE);



  in main:

      Forma f = //creem forma cumva.
      Image i = //creem un Image cumva...
      Poza p = creaza o Poza cu i si f;
      p.deseneaza();
      i.write("poza.png");
     
```
