# Verifica coloanele unei matrici

Creaza o functie care verifica daca toate elemente de pe o coloana a unei matrici de 3x3 contine cifra 1:

de exemplu:
functia va returna `true` pentru:

```
100      010      001     010
100 sau  010 sau  001 sau 011 etc.
100      010      001     010
```

Pentru orice alte matrice care nu au pe cel putin o linie cu cifra 1, functia va retrunea `false`

Apelati functie pe 3 exemple de matrice care retruneaza `true` si 3 exemple pentru care returneaza `false`

### Pseudocod

Asta ii tema, no code this time...
