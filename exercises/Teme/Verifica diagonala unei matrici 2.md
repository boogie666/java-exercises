# Verifica diagonala unei matrici

Creaza o functie care verifica daca toate elemente de pe diagonala secundara a unei matrici de 3x3 contine cifra 1:

de exemplu:
Pentru matricea urmatoare, functia va returna `true`
```
001
010
100
```

Pentru matricea urmatoare functia va returna `false`

```
001
010
000
```

Pentru a putem resolva problema in doua moduri.

1) putem observa ca matricea este de 3x3 tot timpu, astfel rezolvand problema specific pentru o matrice de 3x3
2) putem rezolva generic problema, astfel rezolvand problema pentru pentru orice matrice patratica.


### Pseudocod 1

Solutie specifica pentru o matrice de 3x3:

TEMA!!!! NO CODE FOR YOU!!!

### Pseudocod 2

Solutia generica pentru orice matrice patratica

TEMA!!!! NO CODE FOR YOU!!!!
