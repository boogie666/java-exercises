# Design Patterns


### Definitie


In dezvoltarea de software, un `design pattern` este, in general, o solutie reutilizabila pentru a resolva o problema
care apare, relativ, des...
Un `Design Pattern` reprezinta un sablon pentru a resolva o problema, nu cod concret...


Exista mai multe tipuri de `design patterns` 
Principalele sunt:

  * Creational - sabloane pentru a crea obiecte (nu am facut pana acum)
  * Behavioural - sabloane pentru a modifica modul de functionare a obiectelor (am facut 1 pana acum)
  * Structural - sabloane pentru a adapta/modifica structura obiectelor. (am facut 2 pana acum)


### Behavioural Design Patterns

Definesc o serie de sabloane pentru a modifica comportamentul a unui obiect.

Gasiti [aici](https://en.wikipedia.org/wiki/Behavioral_pattern) mai multe detalii.

#### Strategy Pattern

Acest pattern modifica (la runtime) modul de functionare a unui object

![](/images/design_patterns/strategy_pattern.png)


De exemplu clasa `Layer` contine doua 'strategii':
  * `Filter` - care reprezinta strategia de modificare a fiecarui pixel din poza.
  * `BlendMode` - care reprezinta strategia cu care un `Layer` interactioneaza cu layer-le de sub


Interfata `Filter` defineste o singura metoda (`apply`) iar implementarile concrete (`Grayscale`, `Sepia`, `Saturation` etc..)
definesc care anume este strategia folosita.

Un concept similar se aplica si pentru interfata `BlendMode`.


### Structural Design Patterns

Acest tip de design patterns defineste o serie de sabloane pentru a modifica structura unor clase.

Gasiti [aici](https://en.wikipedia.org/wiki/Structural_pattern) mai multe detalii.

#### Adapter Pattern


Acest pattern structural, permite untilizarea a unei `interface` sa fie folosita ca si cum are fi parte din alt set de `interfete`.

![](/images/design_patterns/adapter_pattern.png)


Pentru a 'interfata' sau 'interopera' intre API-ul `Layer` si API-ul `Image` am creat o clasa (`ImageLayer`) care 
reprezinta o 'punte' intre cele doua biblioteci.

Clasa `ImageLayer` este un `Layer` dar foloseste logica din `Image` pentru a-si face treba.

Putem spune ca `ImageLayer` este un adapter pentru clasa `Image` in API-ul `Layer`


#### Composite Object Pattern

Acest pattern structural, permite tratarea a mai multor obiecte de acelasi tip, ca un sigur obiect din tipul respectiv.

![](/images/design_patterns/composite_object_pattern.png)


Daca ne intoarcem cu gandul al `Formele Geometrice`, observam ca avem `clasa abstracta` (sau mai degraba un `interface`)
numit `Forma`, alaturi de doua forme concrete `Dreptunghi` si `Cerc`.

clasa `Scena` stie sa deseneze o singura forma odata.

Pentru a desena mai multe forme pe ecran, trebuia sa cream un mod de a combina doua `Forme`.
Iar acest mod de a combina forme, trebuia la randul lui, sa fie o `Forma`...

Pentru ne indeplini scopul am creat doua clase noi (`Uninue` si `Intersectie`).

`Uniune` si `Intersectie` sunt exemple ideale a acestui design pattern.


Alt exemplu (inca ne-implementat, dar puteti sa faceti ca tema) este idea a 'filtru compus'.

Avand interfata `Filter` si o serie de implementari posibile pentru filtre.

Putem creea o implementare noua care sa reprezinte mai multe filtre aplicate le acelasi `Layer`

```
  clasa CompositeFilter care implementeaza interfata Filter:
    - List<Filter> lista = new ArrayList<Filter>();

    + addFilter(f: Filter): void
        lista.add(f);    

    @Override
    + apply(c: Color): Color
        Color accumulator = c;
        pentru fiecare f din lista:
          accumulator = f.apply(accumulator);
        retunreaza accumulator;

```

Astfel putem folosii mai multe filter pe un singur `Layer` din `Workspace`-ul nostru...


Acceasi idee se poate aplica si pentru `BlendMode`. 



### Concluzii

'Design pattern'-urie ne permit sa manipulam codul in moduri creative atat pentru a pastra codul de baza simplu,
dar si pentru a adauga functionalitati noi la codul existent.

Putem observa (in cazul `Filter` si `BlendMode`) ca, chiar daca `Filter` si `BlendMode` sunt strategii care modifca
functionalitatea din `Layer`, putem aplica si alte design patterns pentru a implementa aceste strategii.

Un design pattern nu exista izolat, ele este doar o parte din procesul de dezvoltare, care se poate combina cu alte parti.

Pe langa sabloane si moduri creative de a reutiliza codul, design pattern-urile ne ofera un vocabular comun, ca noi (programatorii) 
sa putem exprima idei la un nivel mai inalt.

