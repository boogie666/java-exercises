# Media aritmetica a cifrelor unui numar.

Determinati media aritmetica a cirelor unui numar

Ex: Pentru numarul 7935 cifra cea mai mare ste 6.


Media artimetica pentru exemplu de mai sus se calculeaza in felul urmator:
```
  (7 + 9 + 3 + 5) / 4 = 6
```
Calculul de mai sus reprezinta suma cifrelor imparti la numarul de cifre.



Pentru a rezolva problema, trebuie sa interam prin fiecare cifra din numar.
Simultan trebuie sa numaram cate cifre are numarul.

Putem observa urmatoarele:
  * Pentru un numar 'x', x % 10 ne da ultima cifra din numar.
  * Pentru un numar 'x', x / 10 (in domeniul 'int') elimina ultima cifra din numar.

Putem insuma cifrele dintr-un numar in felul urmator:

1) extragem ultima cifra din numar si o aduam la un total.
3) eliminam ultima cifra.
4) daca numarul a ajuns la 0 ne oprim, altfel o luam de la capat.

Simultan pentru de fiecare daca cand o luam de la capat, incrementan un contor.
La final vom aveam suma cirelor si numarul de cifre.

Ne mai ramane sa impartim o valoare la cealalta si am terminat.


### Pseudocod

```
  fie n = 7934,
      suma = 0,
      numarDeCifre = 0;

  cat timp n > 0:
    fie ultimaCifra = n % 10;
    suma = suma + ultimaCifra.
    numarDeCifre = numarDeCifre + 1;
    n = n / 10;
  
  fie mediaCifrelor = suma / numarDeCifre;  

  afiseaza mediaCifrelor;

```
