# Cea mai mare cifra dintr-un numar

Determinati care este mai mai mare cifra dintr-un numar.

Ex: Pentru numarul 7934 cifra cea mai mare ste 9


Pentru a rezolva problema, trebuie sa interam prin fiecare cifra din numar.

Putem observa urmatoarele:
  * Pentru un numar 'x', x % 10 ne da ultima cifra din numar.
  * Pentru un numar 'x', x / 10 (in domeniul 'int') elimina ultima cifra din numar.

Putem determina cifra maxima din numar in felul urmator:

1) extragem ultima cifra din numar
2) determinam daca ultima cifra este cea mai mare de pana acum.
3) eliminam ultima cifra.
4) daca numarul a ajuns la 0 ne oprim, altfel o luam de la capat.


### Pseudocod

```
  fie n = 7934,
      max = -1;

  cat timp n > 0:
    fie ultimaCifra = n % 10;
    daca ultimaCifra este mai mare decat max:
      max = ultimaCifra.
    
    n = n / 10;

  afiseaza max;

```
