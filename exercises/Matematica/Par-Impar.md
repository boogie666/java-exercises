# Par-Impar

Verificati daca urmatoarele numere sunt pare:
1, 4, 123, 6

Un numar este par daca si numai daca 
restul impartii lui la 2 este 0.

Restul impartii, in java, se calculeaza cu operatrul %

Result impartii a orice numar 'x' cu un alt numar 'n' are valori intre 
0 si n - 1.


Adica:
   * x % 10 poate sa aibe una din valorile 0,1,2,3,4,5,6,7,9
   * x % 6 poate sa aibe una din valorile 0,1,2,3,4,5
   * x % 3 poate sa aibe una din valorile 0,1,2

Restul impartii are unui numar 'x' la alt numar 'y' este 0 daca 
x se imparte exact la y.


Un numar este par daca se imparte exact (sau este multiplu de) 2.

spunem ca 'a' este par daca a%2 == 0
spunem ca 'a' este impar daca a%2 != 0 (sau a%2==1)

### Pseudocod

``` 
    // pentru fie care numar.
    fie  n = unu din numerele de mai sus;

    fie iiPar = n % 2 == 0    

    afiseaza "Este " + n " par? -> " + iiPar;
```


