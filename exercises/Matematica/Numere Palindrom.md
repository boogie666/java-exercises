# Numere Palindrom

Scrieti un algoritm pentru a determina daca un numar este palindrom sau nu.


### Ce este un numar palindrom?

Un numar este palindrom daca citit de la stanga la dreapta sau de la dreapta la stanga este la fel.

De exemplu: 
  * 121 este palindrom
  * 123 nu este palindrom
  
  * 123321 este palindrom
  * 123324 nu este palindrom

Practic, un numar este palidrom daca el este egal cu inversul lui.


Putem oberserva urmatoare proprietati ale numerelor.


 * daca impartim un numar la 10 (in domeniul numerelor intregi) ii acelasi lucru cu a taia ultima cifra din el.
 * daca luam restul impartiri unui numar la 10 ii acelasi lucru cu a extrage ultima cifra din el.
 * daca inmultim un numar cu 10, ii acelasi lucru cu a adauga un zero la final.
 * si 0 + orice numar este acel numar.


Astfel putem deduce urmatorul algoritm.


Pentru a inversa un numar trebuie sa:
  * declaram un numar 'rezultat' initial cu valoarea 0.
  * sa extragem ultima cifra din numarul nostru
  * sa aduam aceasta cifra la rezultat
  * sa impartim numarul nostru cu 10 (asftel eliminand ultima cifra)
  * sa inmultim rezutatul cu zece, (ca sa avem unde sa punem urmatoarea cifira)
  * sa repetam asta pana nu mai avem cifre (adica pana cand numarul nostru ii 0)



### Pseudocod

```
  fie N = 121, // sau orice numar care dorim sa il verificam.
      rezultat = 0,
      copie = N; // dorim sa tinem o copie a numarul ca sa avem cu ce sa verificam.


  cat timp N nu este 0:
    fie ultimaCifra = N % 10;
    N = N / 10;
    rezultat = rezultat * 10 + ultimaCifra;


  daca rezultat ii egal cu copie: 
    afiseaza "Numarul " + copie + " este palindrom"
  altfel:
    afiseaza "Numarul " + copie + " NU este palindrom"
```
