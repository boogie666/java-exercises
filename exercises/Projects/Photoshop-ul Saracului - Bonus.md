# Photoshop-ul Saracului - Bonus
## Bonus

### Filtre Bonus


Fiecare operatie de filtrare este definita ca o operatie pe canalele de culoare ale fiecarui pixel din imagine:

Filtrul Grayscale este definit ca:

```
  public class Grayscale implementes Filter {
    @Override
    public Color apply(Color c){
        int media = (c.getRed + c.getGreen() + c.getBlue()) / 3;
        return new Color(media, media, media, c.getAlpha());
    }
  }
```

Sau mai pe scurt:

```
  media = (c.r + c.g + c.b) / 3;
  color(media, media, media, c.a)
```

Filtrul Invert poate fi definit ca:

```
  public class Invert implements Filter{
    @Override 
    public Color apply(Color c){
      int newRed = 255 - c.getRed(); 
      int newGree = 255 - c.getGreen();
      int newBlue = 255 - c.getBlue();
      int newAlpha = c.getAlpha();
      return new Color(newRed, newGreen, newBlue, newAlpha);
    }
} 

```

sau mai pe scurt:

```
  r = 255 - c.r;
  g = 255 - c.g;
  b = 255 - c.b;
  a = c.a;
  color(r,g,b,a)
```

Filtrul brightness poate fi definit ca:

```

public class Brightness implements Filter{
  private int value;
  
  public Brightness(int value){
    this.value = value;
  }
  
  @Override
  public Color apply(Color c){
    int newRed = Math.max(0, Math.min(255, c.getRed() + this.value));
    int newGreen = Math.max(0, Math.min(255, c.getGreen() + this.value));
    int newBlue = Math.max(0, Math.min(255, c.getBlue() + this.value));
    return new Color(newRed, newGreen, newBlue, c.getAlpha());
  }
}

```

sau mai pe scurt:

```
  r = max(0, min(255, c.r + value));
  g = max(0, min(255, c.g + value));
  b = max(0, min(255, c.b + value));
  color(r,g,b,c.a);

```

Codul 'mai pe scurt', in mod evident, nu este valid in java, dar
el reprezinta operatiile care se executa pe fiecare pixel...
Exemple de mai sus arata cum se traduce din codul `pe scurt` in java.

Mai exact
  * c.r este echivalent c.getRed()
  * c.g este echivalent c.getGreen()
  * c.b este echivalent c.getBlue()
  * c.a este echivalent c.getAlpha()
  * `c` este presupus sa fie numele parametrului functiei `apply`
  * Orice alta variabla folosita in calcul este presupsa sa fie variabla membru al clasei la care se lucreaza, pasata prin constructor.
  * `color` reprezinta constructor-ul clasei Color (adica new Color(...))
  * functii precum `min`, `max`, `abs` sunt functii din `Math` (adica Math.min, Math.max, Math.abs etc)

Fiecare filtru are sub el imaginea resultat in urma applicari sale pe imaginea de referinta

De aici incolo v-om definii filterele doar in modul prescurtat.
Tranducerea in java ramane ca exercitiu pentru cititor.

Imagine de referinta:

![base image](/images/poor-mans-photoshop/bonus/base.png)

#### Grayscale

```
  media = (c.r + c.g + c.b) / 3;
  color(media, media, media, c.a);
```


![](/images/poor-mans-photoshop/bonus/grayscale.png)

#### Invert

```
  r = 255 - c.r;
  g = 255 - c.g;
  b = 255 - c.b;
  a = c.a;
  color(r,g,b,a)
```

![](/images/poor-mans-photoshop/bonus/invert.png)

#### Brightness

```
  r = max(0, min(255, c.r + value));
  g = max(0, min(255, c.g + value));
  b = max(0, min(255, c.b + value));
  color(r,g,b,c.a);

```
Cu valoare 20:
![](/images/poor-mans-photoshop/bonus/brightness.png)

#### Saturation

Adjustment poate fii o valoare intre -1 si 1 (float)
-1 este echivament-ul filtrului grayscale
+1 supra-satureaza colorile
0  nu face nimic.

Atentie r,g,b,m si adjusment nu sunt int-uri, sunt float-uri is ele trebuie transformate in int la final

```
  m = max(c.r, max(c.g, c.b));
  r = c.r;
  g = c.g;
  b = c.b;
  daca r nu ii m:
    r += (m - r) * adjustment
  daca b nu ii m:
    b += (m - b) * adjustment;
  daca g nu ii m:
    g += (m - g) * adjustment;
  
  r = max(0, min(255, r));
  g = max(0, min(255, g));
  b = max(0, min(255, b));

  color(r, g, b, c.a); 


```
Cu valoare 0.5f
![](/images/poor-mans-photoshop/bonus/saturation.png)




#### Vibrance

Adjustment poate fii o valoare intre -100 si 100 (float)

Atentie variablile nu sunt int-uri, sunt float-uri is ele trebuie transformate in int la final


```
  m = max(c.r, max(c.g, c.b));
  avg = (c.r + c.g + c.b) / 3f;
  amount = (abs(m - avg) * 2 / 255) * -adjustment / 100f;
  r = c.r;
  g = c.g;
  b = c.b;

  daca r nu ii m:
    r += (m - r) * amount;
  daca b nu ii m:
    b += (m - b) * amount;
  daca g nu ii m:
    g += (m - g) * amount;
  
  r = max(0, min(255, r));
  g = max(0, min(255, g));
  b = max(0, min(255, b));

  color(r,g,b,c.a);

```
Cu valoare 50
![](/images/poor-mans-photoshop/bonus/vibrance.png)




#### Contrast

adjustment negativ v-a scadea contrastul imaginii,
adjustment pozitiv v-a creste contrastul imagnii 

```
r = c.r;
g = c.g;
b = c.b;

r /= 255f;
r -= 0.5f;
r *= adjustment;
r += 0.5f
r *= 255f;
r = max(0, min(255f, r));

g /= 255f;
g -= 0.5f;
g *= adjustment;
g += 0.5f
g *= 255f;
g = max(0, min(255f, g));

b /= 255f;
b -= 0.5f;
b *= adjustment;
b += 0.5f
b *= 255f;
b = max(0, min(255f, b));

color(r,g,b, c.a);


```
Cu valoare 1.5f
![](/images/poor-mans-photoshop/bonus/contrast.png)





#### Colorize

Colorize muta culoarea in mod uniform de la culoarea curenta la culoarea data ca param prin constructor ( numita aici 'input').

input este de tip Color,
level este un numar intre 0 si 100

```

r = (c.r + input.r) / 255f * (level / 100f);
r = max(0, min(255, r));

g = (c.g - input.g) * (level / 100f);
g = max(0, min(255, g));

b = (c.b - input.b) * (level / 100f);
b = max(0, min(255, b));

color(r,g,b,c.a);

```
Cu valoare cu Color.BLUE si 75;
![](/images/poor-mans-photoshop/bonus/colorize.png)





#### Sepia


```

r = c.r / 255f;
g = c.g / 255f;
b = c.b / 255f;
outputRed = (r * .393f) + (g * .769f) + (b * .189f)
outputGreen = (r * .349f) + (g * .686f) + (b * .168f)
outputBlue = (r * .272f) + (g * .534f) + (b * .131f)

nr = (int) max(0, min(255, outputRed * 255f));
ng = (int) max(0, min(255, outputGreen * 255f));
nb = (int) max(0, min(255, outputBlue * 255f));

color(nr, nb, ng, c.a);

```
![](/images/poor-mans-photoshop/bonus/sepia.png)



### Blend Modes Bonus



Fiecare BlendMode este o operatie intre doi pixeli suprapusi.

BlendMode-ul `MultiplyBlend` este definit ca

```
  public class MultiplyBlend extends AbstractBlend{
    @Override 
    protected float calculateBlend(float a, float b){
        return a * b;
    }
  }
```

sau mai pe scurt:

```
  a * b
```


BlendMode-ul `AddBlend` este definit ca:

```
  public class AddBlend extends AbstractBlend{
    @Override 
    protected float calculateBlend(float a, float b){
        return a + b;
    }
  } 
```

sau mai pe scurt:

```
a + b
```

BlendMode-ul `ScreenBlend` este definit ca:

```
  public class ScreenBlend extends AbstractBlend{
    @Override 
    protected float calculateBlend(float a, float b){
        return 1 - (1 - a) * (1 - b);
    }
  } 

```


sau mai simplu:

```
1 - (1 - a) * (1 - b);
```


Restul BlendMod-urilor vor fi prezentate doar ca variata `simpla`
tranducerea lor in java ramane ca exercitiu al cititorului.

Toate filterele vor fi applicate pe urmatoarele doua imagini de referinta:

![](/images/poor-mans-photoshop/bonus/blend_base1.png)
![](/images/poor-mans-photoshop/bonus/blend_base2.png)


#### Add

```
a + b
```

![](/images/poor-mans-photoshop/bonus/blend_add.png)

#### Subtract

```
a - b
```

![](/images/poor-mans-photoshop/bonus/blend_subtract.png)

#### Multiply

```
a * b
```

![](/images/poor-mans-photoshop/bonus/blend_multiply.png)
#### Divide

```
a / b
```

![](/images/poor-mans-photoshop/bonus/blend_divide.png)

#### Overlay

```
a * (a + b * 2 * (1 - a))
```

![](/images/poor-mans-photoshop/bonus/blend_overlay.png)

#### SoftLight

```
daca b > 0.5f:
  1 - ((1 - b) * (1 - (a - 0.5)))
altfel:
  b * (a + 0.5)

```

![](/images/poor-mans-photoshop/bonus/blend_softlight.png)

#### Lighten

```
  daca a > b:
    a
  altfel:
    b
```


![](/images/poor-mans-photoshop/bonus/blend_lighten.png)

#### Darken
```
  daca a > b:
    b
  altfel:
    a
```

![](/images/poor-mans-photoshop/bonus/blend_darken.png)


### Layer bonus


Pe langa filtre si moduri de compozitare putem avea si alte feluri de layer-e.

Cum avem SolidLayer si ImageLayer putem extinde clasa Layer pentru a creea alte layere...

Pentru a face clasa `ImageLayer` am facut

```
  public class ImageLayer extends Layer{
    private Image image;

    public ImageLayer(int x, int y, Image image){
      super(x, y, image.getWidth(), image.getHeight());
      this.image = image;
    }

    @Override
    public Color getPixel(int i, int j){
       return this.image.getPixel(i, j);
    }
  }

```

#### GradientLayer

Putem crea urmatorul fel de layer

![](/images/poor-mans-photoshop/bonus/gradient-horizontal.png)

daca la metoda getPixel returnam colorea bazat pe `i`-ul pixel-ului

Astfel vom obtine un gradient de la negru la alb pe orizontala.
```
  public class HorizontalGradientLayer extends Layer {
     private int width;

     public HorizontalGradientLayer(int x, int y, int width, int height){
        super(x, y, width, height);
        this.width = width;
     }

     @Override 
     public Color getPixel(int i, int j){
        float procent = (float) i / this.width; // asa v-om transforma intervalul '0-width) la intervalul (0-1)
        float colorValue = procent * 255f; // asta transformam intervalul 0-1 la intervalul 0-255
        int value = (int) colorValue; //clasa color accepta `int`-uri ca parametri.
        return new Color(value, value, value);
     }
  }
```

sau putem face acelasi lucru pe verticala, daca folosim `height` si `j` in loc de `width` si `i`


```
  public class VerticalGradientLayer extends Layer {
     private int height;

     public VerticalGradientLayer(int x, int y, int width, int height){
        super(x, y, width, height);
        this.height = height;
     }

     @Override 
     public Color getPixel(int i, int j){
        float procent = (float) j / this.height; 
        float colorValue = procent * 255f;
        int value = (int) colorValue; 
        return new Color(value, value, value);
     }
  }

```

![](/images/poor-mans-photoshop/bonus/gradient-vertical.png)
