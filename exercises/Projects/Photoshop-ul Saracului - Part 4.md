# Photoshop-ul Saracului - Part 1
## Workspace-ul

Uitandu-ne la codul din Main putem observa un pattern.

Observam ca: 

 * Creem o imagine `resultat`
 * Creem mai multe `Layer`-uri
 * Applicam, in ordine, fiecare `Layer` pe `resultat`
 * Salvam imaginea `resultat`

```
Image resultat = new Image(100, 100);

Layer l1 = // primul layer
Layer l2 = // al doilea layer
Layer l3 = // al treilea layer

l1.apply(resultat);
l2.apply(resultat);
l3.apply(resultat);

resultat.save("poza.png");

```

Codul este foarte utilizabil si in starea in care care il avem acum.
Dar putem sa mai adaugam o ultima inbunatatire.

Putem creea o clasa noua (dupa cum este descris si in UML)
numita `Workspace`

acest `Workspace` va sti cum sa creeze imaginea `resultat` si cum sa aplice
`Layer`-ele pe imagine.

```
  public class Workspace {
     private int width;
     private int height;
     private List<Layer> layers; // atentie aici 
      
     public Workspace(int width, int height){
        this.width = width;
        this.height = height;
        this.layers = new ArrayList<>(); // atentie aici
     }

     public void addLayer(Layer l){
        this.layers.add(l);
     }

     public void save(String imagePath) throws IOException {
        Image result = new Image(this.width, this.height);
        for(int i = 0; i < this.layers.size(); i++){ // atentie aici
          Layer l = this.layers.get(i);
          l.apply(result);
        }
        result.save(imagePath);
     }
  }

```

Observam ca in implementarea clasei `Workspace` folosim notiunea abstacta `List<Layer>` 
si implemnetarea concreta de `ArrayList<>`.

`List` este o interfata definita in packetul `java.util` si reprezinta o lista...
(ce-i intre `<>` ii ce putem sa punem in lista, in cazul de fata in lista punem `Layer`-e)

`ArrayList`-ul este o posibila implementare a interfetei `List`

Intr-o lista putem adauga elemente cu metoda `add`, putem scoate elemente cu metoda `remove`,
putem afla cate element sunt in lista cu metoda `size`, si putem acesa elemente cu metoda `get`.
(si multe alte operatii)



### Ultima modifcare


Acum ca avem notiunea de `Workspace` putem creea si rula codul din exemplu de utilizare de la inceput.

```

public static void main(String[] args) throws IOException {
  Workspace ws = new Workspace(600, 600);

  Layer image = new ImageLayer(0, 0, Image.load("o_poza.png"));
  image.setFilter(new GrayscaleFilter());
  ws.addLayer(image);

  Layer redOverlay = new SolidLayer(0, 0, 600, 600, Color.RED);
  redOverlay.setBlendMode(new MultiplyBlend());
  ws.addLayer(redOverlay);

  ws.save("poza_modificata.png");
  
}

```
