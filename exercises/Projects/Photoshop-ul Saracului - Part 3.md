# Photoshop-ul Saracului - Part 3
## BlendModes


Un "Blend Mode" este modul de interactione a unui Layer cu Layerele de sub el.
Practic `BlendMod`-ul este o functie matematica care se aplica pe doi pixeli suprapusi si
produce valoarea finala a pixelul de la pozitia respectiva.


Putem definii clasa BlendMode in felul urmator (la inceput concret, mai tarziu abstract)

Pentru implementarea initiala v-om crea un mod de blending care ne da culoarea layer-ui de desupra
astfel daca suprapunem pozele:

![pisica](/images/poor-mans-photoshop/cat.png)

si

![patrat rosu](/images/poor-mans-photoshop/patrat_rosu.png)

v-om obtine:

![Resultat](/images/poor-mans-photoshop/simple_blend.png)

```

clasa BlendMode():
  Color apply(Color a, Color b):
    return b;

```

Putem folosii `BlendMode`-ul in felul urmator:

In clasa Layer trebuie sa adaugam un camp pentru blend mode:

```
  public abstract class Layer{

     //... cod existent
    
     private BlendMode blendMode;

     //... cod existent

     public void setBlendMode(BlendMode b){
        this.blendModel = b;
     }
     
  }
```

Iar metoda `apply` din clasa `Layer` trebuie modificata in felul urmator:

din asta:

```
  metoda apply(img):
    pentru fiecare i de la 0 la this.width:
      pentru fiecare j de la 0 la this.height:
        daca pozitieCorecta(i + x, j + y, img.getWidth, img.getHeight):
          img.putPixel(i, j, aplicaFiltru(this.getPixel(i+x, j+y))); // aici e modificarea
 

```

devine:

```
  metoda apply(img):
    pentru fiecare i de la 0 la this.width:
      pentru fiecare j de la 0 la this.height:
        daca pozitieCorecta(i + x, j + y, img.getWidth, img.getHeight):
          Color pixelCuFiltru = aplicaFiltru(this.getPixel(i+x, j+y)); // extragem rezutatul metodei aplicaFiltru variablia
          Color pixelCuBlend = aplicaBlend(image.getPixel(i+x, j+y), pixelCuFiltru); // aplicam blendul pe pixelul current de la i/j si pixel-ul cu filter
          img.putPixel(i, j, pixelCuBlend); // punem pixel-ul nou creat in imaginea rezultata...
  

  metoda aplicaBlend(Color oldColor, Color newColor):
    returneaza this.blendMode.apply(oldColor, newColor);  
 
```


Acum putem folosii layer-ele asa:

```

Image pisica = Image.load("pisica.png");

Image resultat = new Image(pisica.getWidth(), pisica.getHeight());

Layer l1 = new ImageLayer(0, 0, pisica);
l1.setBlendMode(new BlendMode());

int laturaPatrat = 50;
Layer l2 = new SolidLayer(
  pisica.getWidth() / 2 - laturaPatrat / 2,  // punem patrat la mijlocul pozei pe lungime
  pisica.getHeight() / 2 - laturaPatrat / 2, // si pe inatime,
  laturaPatrat, 
  laturaPatrat,
  Color.RED
);
l2.setBlendMode(new BlendMode());

l1.apply(resultat);
l2.apply(resultat);


resultat.save("poza.png");

```

### Imbunatatire #1


Acum ca avem clasa `BlendMode` definita concert, este timpul sa abstractizam.

```
  public class BlendMode{
    public Color apply(Color a, Color b){
      return b;
    }
  }
```

devine:

```
  public interface BlendMode {
    Color apply(Color a, Color b);
  }
```

Si putem creea o noua clasa numita `SimpleBlend` care implementeaza 
interfata `BlendMode` (cu implementarea de pana acum)


```
  public class SimpleBlend implements BlendMode {
      @Overrride
      public Color apply(Color a, Color b){
          return b;
      }
  }

```

Am fi putut face o clasa abstracta din `BlendMode` in loc de interfata.
Dar stim ca `BlendMode` nu va avea decat o singura metoda abstracta numita `apply`

Deci o interfata este, nu doar suficienta, dar si dorita in cazul acesta.


### Imbunatatire #2

Putem observa ca trebuie sa aplem metoda `setBlendMode` pe fiecare layer.
Daca nu facem aia, codul va da un `NullPointerException` in metoda `aplicaBlend`...

Acest lucru nu este de dorit...

Noi dorim ca acest `SimpleBlend` sa fie cazul implicit.
Adica daca nu setam nici un blend mode, codul trebuie sa se comporte ca si cum am fi setat 
`SimpleBlend` ca blend mode...

Putem sa facem urmatoarea modificare.

In constructorul clasei abstracte `Layer` putem initializa variabila `blendMode` pe o noua instanta
a clasei `SimpleBlend`

```

public abstract class Layer {
   // cod de pana acum
   private BlendMode blendMode;

   // constructor-ul modificat
   public Layer(/* parametrii de pana acum */){
      // cod de pana acum
      this.blendMode = new SimpleBlend();
   }

   // mai mult cod
}

```

Acum putem sa eliminam apelurile la `setBlendMode` din Main...

```

Image pisica = Image.load("pisica.png");

Image resultat = new Image(pisica.getWidth(), pisica.getHeight());

Layer l1 = new ImageLayer(0, 0, pisica);
//l1.setBlendMode(new BlendMode()); cod comentat

int laturaPatrat = 50;
Layer l2 = new SolidLayer(
  pisica.getWidth() / 2 - laturaPatrat / 2,  // punem patrat la mijlocul pozei pe lungime
  pisica.getHeight() / 2 - laturaPatrat / 2, // si pe inatime,
  laturaPatrat, 
  laturaPatrat,
  Color.RED
);
//l2.setBlendMode(new BlendMode()); cod comentat

l1.apply(resultat);
l2.apply(resultat);


resultat.save("poza.png");

```


### Alte moduri de compozitare


Putem definii alte moduri de compozitare implementand interfata `BlendMode`
in alte moduri...

de exemplu:

MultiplyBlend - care inmulteste cele doua coluri, avand ca rezultat o intesificare a culorilor inchise.


Avand ca resultat:
![pisica](/images/poor-mans-photoshop/multiply_blend.png)



```

  public class MultiplyBlend implements BlendMode{
    @Override
    public Color apply(Color a, Color b){
      return new Color(
        blend(a.getRed(), b.getRed()),
        blend(a.getGreen(), b.getGreen()),
        blend(a.getBlue(), b.getBlue()),
        alphaBlend(a.getAlpha(), b.getAlpha())
      );
    }

    private int blend(int a, int b){
      float fa = a / 255f; // aducem valorile in intervalul 0-255 la intervalul 0-1
      float fb = b / 255f;
      float result = fa * fb; // aici inmultim valorile 
      // avem grija ca resultatul sa nu iasa din interval-ul 0-1
      if(result > 1) {
        result = 1;
      }
      if(result < 0){
        result = 0;
      }
      
      // transformam rezultatul din intervalul 0-1, inapoi la intervalul 0-255
      return (int) (result * 255);
    }

    private int alphaBlend(int a, int b){
        float fa = a / 255f;
        float fb = b / 255f;
        float result = fa + fb * ( 1 - fa ); // tot ii la fel, dar difera cel calcul facem
        if(result > 1) {
          result = 1;
        }
        if(result < 0){
          result = 0;
        }
        return (int) (result * 255); 
    }
  }


```

AddBlend - care aduna culorile, avand ca rezultat estomparea culorilor inchise si intesificarea culorilor deschise:

![pisica](/images/poor-mans-photoshop/add_blend.png)



```

  public class AddBlend implements BlendMode{
    @Override
    public Color apply(Color a, Color b){
      return new Color(
        blend(a.getRed(), b.getRed()),
        blend(a.getGreen(), b.getGreen()),
        blend(a.getBlue(), b.getBlue()),
        alphaBlend(a.getAlpha(), b.getAlpha())
      );
    }

    private int blend(int a, int b){
      float fa = a / 255f; // aducem valorile in intervalul 0-255 la intervalul 0-1
      float fb = b / 255f;
      float result = fa + fb; // aici adunam valorile 
      // avem grija ca resultatul sa nu iasa din interval-ul 0-1
      if(result > 1) {
        result = 1;
      }
      if(result < 0){
        result = 0;
      }
      
      // transformam rezultatul din intervalul 0-1, inapoi la intervalul 0-255
      return (int) (result * 255);
    }

    private int alphaBlend(int a, int b){
        float fa = a / 255f;
        float fb = b / 255f;
        float result = fa + fb * ( 1 - fa ); // tot ii la fel, dar difera ce calcul facem
        if(result > 1) {
          result = 1;
        }
        if(result < 0){
          result = 0;
        }
        return (int) (result * 255); 
    }
  }


```


Putem observa ca MultiplyBlend si AddBlend sunt practic identice.
doar o singura linie este diferita intre ele, si anume:

```
  float result = fa * fb;
```
  sau
```
  float result = fa + fb;
```


Asta ne duce la inca o imbunatatire a codului...

Putem crea inca o clasa abstracta numita `AbstractBlendMode`
pentru a incaplusa toata functionalitea comuna intre `BlendMode`-uri...


```
  public abstract class AbstractBlendMode implements BlendMode{
    @Override
    public Color apply(Color a, Color b){
      return new Color(
        blend(a.getRed(), b.getRed()),
        blend(a.getGreen(), b.getGreen()),
        blend(a.getBlue(), b.getBlue()),
        alphaBlend(a.getAlpha(), b.getAlpha())
      );
    }

    private int blend(int a, int b){
      float fa = a / 255f; 
      float fb = b / 255f;
      float result = calculateBlend(fa, fb); // aici ii diferenta intre moduri, deci aici putem aveam o metoda abstracta
      if(result > 1) {
        result = 1;
      }
      if(result < 0){
        result = 0;
      }
      
      return (int) (result * 255);
    }

    private int alphaBlend(int a, int b){
        float fa = a / 255f;
        float fb = b / 255f;
        float result = fa + fb * ( 1 - fa );
        if(result > 1) {
          result = 1;
        }
        if(result < 0){
          result = 0;
        }
        return (int) (result * 255); 
    }
  }
  
  protected abstract float calculateBlend(float fa, float fb);

```


Acum clasele MultiplyBlend si AddBlend pot extinde aceasta clasa abstract

```
  public class MultiplyBlend extends AbstractBlendMode{
     @Override
     public float calculateBlend(float fa, float fb){ 
        return fa * fb;
     }
  }

```

```
  public class AddBlend extends AbstractBlendMode{
     @Override
     public float calculateBlend(float fa, float fb){ 
        return fa + fb;
     }
  }

```


Acum putem intr-un mod trivial implementa alte moduri de compozitare...
Pentru a vedea celeante calcule vedeti sectuinea `Bonus`
