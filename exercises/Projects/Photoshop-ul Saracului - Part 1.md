# Photoshop-ul Saracului - Part 1
## Layers


Un `Layer` reprezinta o imagine (fie de pe disk, fie generata prin cod)
Observam sa `Layer`-ul definit in diagrama UML este reprezantat printr-o clasa abstracta.
`Layer`-ul contine o serie de proprietati, o serie de metode concrete si o singura metoda abstracta.

Prorietatie sunt:

  * x, y, width, height - toate de tip intreg, care prezinta pozita unui layer in cadrul unui workspace.
  * filter - o referinta la un membru al familiei de clase `Filter` - care reprezinta filtrul aplicat asupra acestui layer
  * blendMode - o referinta la membru al fimiliei de clase `BlendMode` - care reprezinta modul in care acest layer este aplicat peste alte layere.


Metodele concrete sunt:

  * setFilter - care va seta proprietatea filter pe ce primeste ca parametru.
  * setBlendMode - care va seta proprietatea blendMode pe ce primeste ca parametru.
  * apply - care va aplica filtrul si blendMode-ul pe Image-ul primit ca parametru.

Metoda abstracta este:

  * getPixel - care ne va da pixel-ul correct pentru layer-ul current la pozitia i si j.


Initial o sa face `Layer`-ul ca si clasa concreta pentru a putea folosii clasa direct...
(si sa testam ca totul merge correct)

Ulterior v-om face clasa Layer abstracta, si v-om extrage logica din metoda getPixel in sub-clase
fiecare cu un rol specific.

### Mod de folosire de baza - fara nici un filtru sau blendMode

```
public static void main(String[] args){
  Layer l = new Layer(0,0, 800, 600);

  Image i = new Image(1000, 1000);

  l.apply(i);  

  i.write("poza_mare.png");
}

```

 
### Clasa Layer, basic version


```java
  public class Layer{
    private int x, y, width, height;
    
    public void apply(Image img){
      for(int i = 0; i < width; i++){
        for(int j = 0; j < height; j++){
          img.putPixel(i, j, this.getPixel(i, j));
        }
      }
    }

    public Color getPixel(int i, int j){
        return Color.RED; // pentru moment, nu ne intereseaza metoda getPixel, asa ca o facem sa creeze un 'layer' plin cu culoarea rosie
    }
  }
```


Daca testam codul acesta vedem ca o sa avem o zona rosie de 800x600 sus in colt-ul unei poze de 1000x1000



### Pozitionare

Observam ca, clasa Layer nu foloseste de loc coordonatele sale x/y...
Mai observam ca metoda apply primeste o poza ca parametru si pune pixelii in poza apply la coordonatele i/j.
dar i si j incep de la zero.


Putem folosii aceasi metoda de translatare ca la Forme...


In loc sa punem pixel-ul la i/j putem sa punem pixel-ul la i + x si j + y...

```
  img.putPixel(i + x, j + y, this.getPixel(i, j));
``` 

Re-runand codul din main, observam ca nu s-a schimbat nimic pentru ca x/y de la Layer este 0/0

Daca modificam sa fie 20/20 vedem ca layer-ul nostru s-a mutat mai in jos...

Ce se intampla daca punem layer-ul nostru la pozitia -20/-20?

Ne-am astepta sa fie mai sus de 0,0 (practic un pic iesit din ecran)...
Dar daca rulam asa programul, vedam ca programul arunca o exceptie (adica da eroare)

De ce?

Clasa `Image` din biblioteca `pixels` este doar o simpla matrice.
Si la orice array (indiferent de numarul de dimensiuni) nu putem accesa indici decat intre 0 si lungimea array-ului.

Codul nostru din `apply` este correct, dar nu in toate cazurile...

Trebuie sa ingoram cazurile cand `i + x` si `j + y` sunt mai mici de 0 si mai mari decat lunginea, respectiv inaltimea, pozei.

```java
  public void apply(Image img){
    for(int i = 0; i < width; i++){
      for(int j = 0; j < height; j++){
        if(inBounds(i + x, j + y, img.getWidth(), img.getHeight())){
          img.putPixel(i + x, j + y, this.getPixel(i, j));
        }
      }
    }
  } 

  private boolean inBounds(int i, int j, int width, int height){
    if( i < 0 || i >= width){
      return false;
    }
    if(j < 0 || j >= height){
      return false;
    } 
    return true;
  }
```

Ruland Main acum vedem ca poza sa mutat corect si eroara a disparut.


### Layer-ul abstract.

Specificatia (adica diagrama UML) zice ca layer-ul este o clasa abstract cu o metoda abstract getPixel...
Putem face asta acum...


```java
  public class Layer{
```

devine 


```java
  public abstract class Layer{

```

si 

```java
  public Color getPixel(int i, int j){
    return Color.RED;
  }
```

devine

```java
  public abstract Color getPixel(int i, int j);
```


Observam ca dupa schimbarea aceasta in cod avem errori de compilare in Main.
Java nu poate sa instantieze o clasa abstracta.


Putem face inca o clasa (concreta de data asta) numita SolidLayer.

Aceasta clasa reprezinta un layer umplut cu o singura culoara (primata ca parametru in constructor).

```java
  public class SolidLayer extends Layer{
     private Color fillColor;
     public SolidColor(int x, int y, int width, int height, Color fillColor){
        super(x, y, width, height);
        this.fillColor = fillColor;
     }
     
     @Override
     public Color getPixel(int i, int j){
        return this.fillColor;
     }
  }

```


Acum in Main putem sa facem urmatoarea modificare pentru a face codul sa compileze:


```java
  Layer l = new Layer(0,0, 800, 600);

```

devine

```java
  Layer l = new SolidLayer(0, 0, 800, 600, Color.RED);
```


### ImageLayer 

In continuare putem crea clasa ImageLayer care va stii sa afiseze pixelii dintr-o poza.
V-om definii clasa ImageLayer doar in functie de pozitia layer-ul si o poza (width-ul si height-ul il v-om lua din poza)

```
public class ImageLayer extends Layer{
  private Image image;
  
  public ImageLayer(int x, int y, Image image){
    super(x, y, image.getWidth(), image.getHeight());
    this.image = image;
  }

  public Color getPixel(int i, int j){
    return this.image.getPixel(i, j);
  }
}


```

Observam ca in constructor, trebuie sa apelam constructor-ul clasei de baza (adica `Layer`)
constructor-ul clasei `Layer` asteapta de la noi 4 parametri (x, y, width si height)

Noi vrem ca `ImageLayer`-ul sa aibe dimensinuea pozei incarcate.
Asa ca ii pasam ca parametri la super clasa image.getWidth() pe pozitia width-ului 
si image.getHeight() pe pozitia height-ului.



Acum in Main putem sa incarcam o poza astfel

```
Layer l = new ImageLayer(0, 0, Image.load("o_poza.png"));

```

Bine inteles ca putem muta poza incarcata oriunde in poza generata, schimband x/y-ul pasat ca  parametru in constructor.



