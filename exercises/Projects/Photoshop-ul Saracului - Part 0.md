# Photoshop-ul Saracului

Creati un program de editare de poze.



### Features 

Programul trebuie sa suporte:
  * Incarcare/savare de imagini.
  * Compozitare de imagini. (suparpunere de doua sau mai multe imagini, in diferite moduri)
  * Editare prin aplicare de unu sau mai multe filter.

### Diagrama UML

O diagrama UML este reprezantare grafica a claselor care fac parte dintr-un program.
Diagrama UML este similara cu planul unei case.

Similar cu planul casei, diagrama UML arata clasese dintr-un program la un nivel inalt,
fara sa dea detalii de implementare.

Pentru proiectul acesta avem urmatoarea diagrama UML:

![Diagrama UML](/images/poor-mans-photoshop-uml.png)


Putem obvserva ca diagrama contine toate lucrurile necesara pentru a implementa
feature-urile cerute.

Clasa `Workspace` reprezinta suprafata de lucru cu o poza.

`Workspace`-ul se foloseste de mai multe concepte abstracte pentru as indeplini job-ul.
Un `Workspace` este compus din 0 sau mai multe `Layer`'s...

Un `Layer` reprezinta un 'strat' din lucrarea final asupra caruia putem sa aplicam `Filter`'s...
Un `Layer` poate fii, o Imagine, un Fill, un gradient etc...

Un `Filter` reprezinta o transformare a unei imagini. (de exemplu Grayscale, Sepia, Contrast etc...)

Pe langa un filtre, un `Layer` trebuie sa defineasca modul in care el se suprapune peste `Layer`-ul 
anterior. El poate face folosind un alt concept abstract numit `BlendMode`... (asta ii compozitare)

Putem deduce ca `BlendMode`-ul si `Filter`-ul sunt optionale.
Astfel un `Layer` fara un `Filter` applicat trebuie sa foloseasca datale originale de imagine,
fara modificari.

Si un `Layer` fara un `BlendMode` applicat trebuie sa se aplice direct peste `Layer`-ul anterior
fara modifare (decat poate, respectant canalul alpha al imagini incarcate in el)




### Cod final


Codul de mai jos reprezinta modul de utilizare a programului nostru...

```

public static void main(String[] args){
  Workspace ws = new Workspace(600, 600);

  Layer image = new ImageLayer(0, 0, Image.load("o_poza.png"));
  image.setFilter(new GrayscaleFilter());
  ws.addLayer(image);

  Layer redOverlay = new SolidLayer(0, 0, 600, 600, Color.RED);
  redOverlay.setBlendMode(new MultiplyBlend());
  ws.addLayer(redOverlay);

  ws.save("poza_modificata.png");
  
}


```
