# Photoshop-ul Saracului - Part 2
## Filters

Observam, in UML, ca `Layer`-ul foloseste o clasa abstracta numita `Filter`.
Pentru inceput, v-om ignora faptul ca, clasa `Filter` este abstracta.
si v-om presupune ca `Filter` defapt face poza alb-negru...

Astfel putem defini clasa `Filter` in urmatorul mod.


```
  clasa Filter()
    apply(c - de tip Color):
      media = (c.getRed() + c.getGreen() + c.getBlue()) / 3 
      retunreaza new Color(media, media, media);
```

Clasa aceasta trebuie integrata inpoi in clasa `Layer`


Adaugam un field nou in clasa Layer de tipul `Filter` numit `filter`...
si o metoda pentru a seta `filter`-ul ( o metoda care seteaza o variabila se numeste `setter`)

!!!!!!Atentie!!!!!!! NU FACEM O CLASA NOUA `Layer` O MODIFICAM PE AIA CARE O AVEM DEJA!!!!!!!

```
  public abstract class Layer{
      // cod cod cod ...
      
      private Filter filter;
      
      // cod cod cod...

      public void setFilter(Filter f){
        this.filter = f;
      }   

      //restu' codului
  }

```

Acum, filter-ul trebuie folosit in metoda apply.

Metoda apply devine:

```
  metoda apply(img):
    pentru fiecare i de la 0 la this.width:
      pentru fiecare j de la 0 la this.height:
        daca pozitieCorecta(i + x, j + y, img.getWidth(), img.getHeight()):
          img.putPixel(i, j, this.filter.apply(this.getPixel(i, j)));

```

### Testare si inbunatatire

Daca rula codul din Main observam ca el, din nou da o eroare...
mai exact un `java.lang.NullPointerException`

Eroarea aceasta ne spune ca `this.filter` din clasa `Layer` este `null`
iar noi incercam sa apelam metoda `apply`, de pe o referinta nula...


Putem rezolva problema, temporar, setand filter-ul pentru Layer.

in Main din:

```
   Layer l = new ImageLayer(0, 0, Image.load("o poza.png");
   
   Image i = new Image(1000, 1000);
   l.apply(i);
   i.write("poza_mare.png");

```

devine:

```
   Layer l = new ImageLayer(0, 0, Image.load("o poza.png"));
   l.setFilter(new Filter());   // aici setam filtru la layer-ul 'l'

   Image i = new Image(1000, 1000);
   l.apply(i);
   i.write("poza_mare.png");

```

Daca rulam acum Main-ul, vedem ca avem o poza alb/negru in loc de o poza color...


Solutia asta este buna, dar incompleta...
Intr-o lume ideala, am vrea a putem sa NU punem filtre la poza...


Layer-ul trebuie sa se ocupe de situtia cand nu avem filtru...


in clasa Layer trebuie sa facem o verificare daca Filter-ul exista (adica daca ii diferit de null)
si numa cand exista, sa-l aplicam.


metoda apply din asta:

```
  metoda apply(img):
    pentru fiecare i de la 0 la this.width:
      pentru fiecare j de la 0 la this.height:
        daca pozitieCorecta(i + x, j + y, img.getWidth, img.getHeight):
          img.putPixel(i, j, this.filter.apply(this.getPixel(i, j)));

```
devine asta:

```
  metoda apply(img):
    pentru fiecare i de la 0 la this.width:
      pentru fiecare j de la 0 la this.height:
        daca pozitieCorecta(i + x, j + y, img.getWidth, img.getHeight):
          img.putPixel(i, j, aplicaFiltru(this.getPixel(i, j))); // aici e modificarea
  
  
  metoda privata aplicaFiltru(c de tipul Color):
    daca this.filter este diferite de null:
      returneaza this.filter.apply(c);
    altfel:
      returneaza c; // daca nu avem filter, nu facem nimic


```

Acum filtrul este optinal...
Daca nu il punem, nu se intampla nimic, codul functioneaza ca pana acum.
Daca il punem, el va fi aplicat pe fiecare pixel din poza.


### Abstractizare...

Clasa `Filter` momentan nu este abstracta, ci concreta...
Programul nostru, momentan, nu poate avea decat un singur fel de fitru.
(filtrul Grayscale)...

Dar diagrama UML specifica mai multe feluri de filtre...

Putem facem urmatoarea transformare.

Clasa `Filter` devine abstracta (a fel si metoda apply)

```
public abstract class Filter{
  public abstract Color apply(Color c);
}
```

Si creeam o clasa noua pentru filtrul alb-negru numita GrayscaleFilter unde mutam codul pentru
a face un pixel alb-negru.


```
  clasa GrayscaleFilter() care mosteneste Filter:
    suprascrie apply(c - de tip Color):
      media = (c.getRed() + c.getGreen() + c.getBlue()) / 3 
      retunreaza new Color(media, media, media)
```


!! Observatie !!

In java o clasa abstracta care contine DOAR metode publice abstracte si NIMIC ALTCEVA
se numeste Interfata
Java ofera sintaxa speciala pentru asta, deci clasa Filter se poate scrie mai simplu:

```
public interface Filter{
  Color apply(Color c);
}
```

Daca facem aceasta modificare clasa GrayscaleFilter nu mai poate sa extinda Filter,
pentru ca Filter nu mai este clasa.

Dar poate sa implementeze interfata Filter.

```
  public class Grayscale implements Filter{
      /// tot in rest ii la fel...
  }
```


Putem acum sa implementam orice filtru dorim, in aceaasi mod ca GrayscaleFilter....
Creeam o clasa noua extindem clasa Filter (sau implementam interfata Filter, in functie de ce am ales sa facem mai devreme)
Suprascriem metoda apply, si ii definim implementarea pe care o dorim...

Puteti gasii algorimii pentru mai multe filter in sectiunea `Bonus` a proiectului

