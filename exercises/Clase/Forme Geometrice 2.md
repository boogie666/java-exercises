# Forme Geometrice #2

Creati o matrice patratica NxN care sa aibe mai multe forme desenate in ea.
Matricea va contine '#' unde e spatiu gol si '.' unde se afla formele.

Scena va contine un drepunghi.

### Pseudocod
```
    clasa Drepunghi(x, y, lungime, inaltime):
      continePunct(i, j):
        retunreaza subLiniaDeSus(i,j) si deasupraDeLiniaDeJos(i, j) si 
                   laStangaDeLiniaDinDreapta(i, j) si laDreaptaDeLiniaDinStanga(i, j);
          
        
      subLiniaDeSus(i, j):
        retuneaza j <= y + inaltime;
      
      deasupraDeLiniaDeJos(i,j):
        retuneaza j > y;

      laStangaDeLiniaDinDreapta(i, j):
        retunreaza i > x;
      
      laDreaptaDeLiniaDinStanga(i, j):
        retunreaza i <= x + lungime;

    clasa Scena:
      Drepunghi forma;
      int laturaMatrice;
      contructor(N, dreptunghi):
          laturaMatrice = N;
          forma = dreptunghi;
      
      deseneaza():
        pentru fiecare y de la 0 la laturaMatrice:
          pentru fiecare x de la 0 la laturaMatrice:
             daca forma.continePunct(x, y):
                afiseaza '.'
             altfel:
                afiseaza '#'


      fie functia main:
         Drepunghi d = creaza un Drepunghi nou cu 10, 10, 5 si 7;
         Scena s = creaza o Scena noua cu 20 is d;
         s.deseneaza();
```
