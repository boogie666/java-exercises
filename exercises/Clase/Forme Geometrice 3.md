# Forme Geometrice #3

Folosind formele define in exercitiile 1 si 2 creati o scena care contine
o gogasa incadrata intr-un dreptunghi.


### Pseudocod
```
    clasa Gogoasa:
      Cerc c1;
      Cerc c2;
      constructor(x, y, raza, grosime):
        c1 = creaza un Cerc nou cu x, y, raza;
        c2 = creaza un Cerc nou cu x, y, raza - grosime;

      continePunct(i, j):
        retunreaza c1.continePunct(i, j) si NU c2.continePunct(i, j);
    
    clasa Margine:
      Drepunghi d;
      constructor(lungime, inaltime):
        d = creaza un Drepunghi nou cu 0, 0, lungime - 2, inaltime - 2;

      continePunct(i, j);
        retunreaza d.continePunct(i, j);

    clasa Scena:
      Gogoasa g;
      Margine m;
      int laturaMatrice;
      contructor(N, gogoasa, margine):
          laturaMatrice = N;
          g = gogoasa;
          m = margine;
      
      deseneaza():
        pentru fiecare y de la 0 la laturaMatrice:
          pentru fiecare x de la 0 la laturaMatrice:
             daca m.continePunct(x, y) si NU g.continePunct(x, y);
                afiseaza '.'
             altfel:
                afiseaza '#'


      fie functia main:
         Gogoasa g = creaza o Gogoasa noua cu 10, 10, 7 si 3;
         Margine m = creaza o Margine noua 21 si 21;
         Scena s = creaza o Scena noua cu 21, g si m;

         s.deseneaza();
```
