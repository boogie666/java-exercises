# Forme Geometrice #1

Creati o matrice patratica NxN care sa aibe mai multe forme desenate in ea.
Matricea va contine '#' unde e spatiu gol si '.' unde se afla formele.

Scena va contine un cerc.

### Pseudocod
```
    clasa Cerc(x, y, raza):
      continePunct(i, j):
        retruneaza (i - x) * (i - x) + (j - y) * (j - y) <= raza * raza;

    clasa Scena:
      Cerc forma;
      int laturaMatrice;
      contructor(N, cerc):
          laturaMatrice = N;
          forma = cerc;
      
      deseneaza():
        pentru fiecare y de la 0 la laturaMatrice:
          pentru fiecare x de la 0 la laturaMatrice:
             daca forma.continePunct(x, y):
                afiseaza '.'
             altfel:
                afiseaza '#'


      fie functia main:
         Cerc c1 = creaza un Cerc nou cu 10, 10, 5;
         Scena s = creaza o Scena noua cu 20 is c1;
         s.deseneaza();
```
