# Prefix trie

Un trie este un fel de arbore de obicei multe ramuri in care datele se stocheaza
pe frunze.

Un Prefix Trie este o structura de date de tip trie folosit pentru a stoca prefixurile
cuvintelor.

Cu ajutorul lui putem implementa `text predictiv` similar cu cel intalnit pe telefoane.

## API

Vom incepe cu definitia API-ului.
Vom creea o interfata numita TextPredictor doua metode.
``` 
public interface TextPredictor{
  void index(String word);
  List<String> predict(String prefix);
}
```


## Structure de baza

Similar cu lista simpla inlantuita, Trie-ul va fi compus din o clasa de exterioara, care ne 
va oferii functiile de API, si o clasa interna in care v-om implementa structura.


```
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PrefixTrie implements TextPredictor{

	private Node root;
	public PrefixTrie() {
		root = new Node();
	}
	
	@Override
	public void index(String s) {
		root.insert(s);
	}
	
	@Override
	public List<String> predict(String prefix){
		Node n = root.followPath(prefix);
		if(n == null) {
			return Collections.emptyList();
		}
		return n.values();
	}
}
```


Clasa Node contine, dupa cum vedem 3 metode ajutatoare.

Metoda `insert` care introduce un cuvant in structura.
Metoda `followPath` care navigheaza de la un node pana la altul, in functie de literele din prefix.
Metoda `values` care ne da inapoi toate cuvintele aflate sub nodul curent.


```
public class Node {
  private String value;
  private Node[] children;

  public Node() {
    this.children = new Node[26]; // 26 is the number of lower case letters between 'a' and 'z'
    this.value = null;
  }
  
  public void insert(String s) {
    Node current = this;
    for(char c : s.toCharArray()) {

      Node n = current.children[c - 'a'];
      if(n == null) {
        n = current.children[c - 'a'] = new Node();
      }
      current = n;
    }
    current.value = s;
  }
  
  public Node followPath(String prefix) {
    Node current = this;
    for(char c : prefix.toCharArray()) {
      Node n = current.children[c - 'a'];
      if(n == null) {
        return null;
      }
      current = n;
    }
    return current;
  }
  
  
  public List<String> values(){
    List<String> values = new ArrayList<>();
    if(this.value != null)
      values.add(this.value);
    
    for(int i = 0; i < this.children.length; i++) {
      Node n = this.children[i];
      if(n != null) {
        values.addAll(n.values());
      }
    }
    return values;
    
  }
  
}
```


Observam ca in metoda `insert` iteram print toate literele din prefix
si, cand ajungem la un node care nu exista, creem un nod nou.

In metoda `followPath`, din nou iteram print literele din prefix, 
extragem nodul corespunzator pentru fiecare litera, si daca nu avem nod pentru litera curenta
returnam `null`, oprind iteratia.

Iar metoda `values`, pur si simplu colectoaza toate valorile, atat din nodul curent  cat si toate subnodurile.


## Folosire 


Inainte sa folosim auto complete-ul nostru de cuvinte, trebuie sa il instantiem si
sa adugam niste cuvinte in el.

```
  TextPredictor tp = new PrefixTrie();

  tp.index("hello");
  tp.index("hallo");
  tp.index("help");
  tp.index("horse");

  System.out.println(tp.predict("he"));
```

Daca cautam cuvinte care incep cu "he", vedem ca rezutatele sunt doar "hello", si "help"
Daca cautam cuvinte care incep cu "a", nu avem nimic (pentru nu am indexat nici un cuvant care incepe cu "a")


## Exemplu mai complet.

Puteti gasi [aici](https://gitlab.com/boogie666/text-prediction) care contine si 
o implementare mai naiva a aceeasi interfete, dar si masuratori de performanta pe
peste 300000 de cuvinte.

Acolo putem observam performanta data de aceasta structura de date.
O sa observam acolo, ca pentru fiecare litera in plus data, viteza de cautare creste cu un ordin de magnitudine

(de la ~10ms in varitanta naiva, pana ~0.01ms in varianta optimizata)
