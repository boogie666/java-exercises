# Coada

O coada (sau un Queue in engleza) este o structra de date
de tip `FIFO` (First In First Out) 
Sau altfel spus Primul Venit Primul Servit, ca la magazin...


### Exemplu

Avand literele `a`, `b`, `c` si `d` si coada `q`.

Initial q va fi o coada goala.

``` 
litere: a,b,c si d;

     ___________
q =  
     ___________

```

Dupa adaugam litra `a` in coada, ea arata asa:
```
litere: b,c si d;

     ___________
q =  a 
     ___________

```

Dupa adaugam litra `b` in coada, ea arata asa:
```
litere: c si d;

     ___________
q =  a b
     ___________

```

Dupa adaugam litra `c`, respectiv `d`, in coada, ea arata asa:
```
litere: 

     ___________
q =  a b c d
     ___________

```

Daca scoatem un element din coada ea va arata sa:

```

     ___________
q =  b c d
     ___________

scos: a

```

Daca mai scoatem doua element ea va arata asa:
```

     ___________
q =  d
     ___________

scos: a, b, c

```



### API 

Putem definii urmatoarele operatii de baza pe o coada.

```
  void enqueue(e);   -> Introduce un element nou in coada (la capatul cozii)
  E dequeue();        -> Scoate primul element din coada si da eroare daca coada ii goala.
  E peek();       -> Ne da primul element din coada, dar nu il scoate si da erroare daca coada ii goala
  bool isEmpty(); -> Ne spune daca coada este goala sau nu...
```

### 2 posibile implementari.

In primul rand ne putem definii o interfata numita Queue
care reprezinta API-ul unei cozi..

```
  public interface Queue {
    void enqueue(char e);
    char dequeue();
    char peek();
    boolean isEmpty();
  }
```


#### Coada Inlantuita (LinkedQueue)



Pentru implentarea unei cozi inlantuite ne vom folosii de de clasa Node (de la Stiva)


```
  public class LinkedQueue implements Queue {
     private Node first;
     
     @Override
     public void enqueue(char c){
       if(first == null){
          first = new Node();
          first.value = c;
          return;
       }
       Node last = first;
       while(last.next != null){
          last = last.next;
       } 

       Node newLast = new Node();
       newLast.value = c;
       last.next = newLast;    
     }

     @Override
     public char dequeue(){
        char c = first.value;
        first = first.next;
        return c;
     }

     @Override
     public char peek(){
       return first.value;
     }

     @Override
     public boolean isEmpty(){ 
       return first == null;
     }
  }

```

Putem obervar ca implementarea clasei LinkedQueue este foarte asemanatoare  cu
implementarea clasei LinkedStack.
Metodele `peek` si `isEmpty` sunt identice, iar metoda `dequeue` este identica cu metoda `pop`.

Difera doar metoda `enqueue` (analogul metodei `push` din stiva)

In cazul unei cozi, trebuie sa adaugam la final-ul lantului un element nou (nu la incept, ca la stiva)...

Pentru a face asta, trebuie sa parcurgem langtul pana la penultimul element
(adica tot mergem la urmatorul, cat timp avem un urmatorul la care sa mergem)

Dupa care, atasam un nod nou, ca `urmatorul` la ultimul element.


#### Coada Stivuita (StackedQueue)

In aceasta implementarea a interfetei Queue, vom folosi doua stive.

```
  public class StackedQueue implements Queue {
    private Stack s1;
    private Stack s2;
    
    public StackedQueue(){
      s1 = new LinkedStack();
      s2 = new LinkedStack();
    }
    
    @Override
    public void enqueue(char c){
      while(!s2.isEmpty()){
        s1.push(s2.pop());
      }
      s1.push(c);
    }

    @Override
    public char dequeue(){
      while(!s1.isEmpty()){
        s2.push(s1.pop());
      }
      return s2.pop();
    }
    
    @Override
    public char peek(){
      while(!s1.isEmpty()){
        s2.push(s1.pop());
      }
      return s2.peek();
    }

    @Override
    public boolean isEmpty(){
      return s1.isEmpty() && s2.isEmpty();
    }
  }

```

Putem sa vizualizam aceasta implementare care avand doua cilindre cu doar un capat deschis.
In acest cilindre putem pune si scoate bile (prin capatul deschis)

Daca am lua in cilindru plin cu bile si l-am truna pe jos, am observa ca bilele 
ies din cilindru in ordine inversa fata de cum le-am bagat
Cand vrem sa introducem o bila noua 
in primul rand `turnam` toate bilele din cilindrul `s2` in `s1`
dupa care adugam bila noua in `s1`

Cand vrem sa scoatem o bila.
prima data `turnam` toate bilele din cilindrul `s1` in `s2`
pe urma, scoate cea mai de sus bila din `s2`

Coada este goala atunci ambele cilindre sunt goale.


#### Despre implementari multiple

Daca folosim clasele LinkedQueue si StackedQueue, vedem, ca din exteriror, (adica fara se ne uitam la implementare)
aceste doua clase sunt echivanente in rezultat.

Ambele indeplinesc promisiunea facuta de interfata `Queue`, si anume idea de `Primul venit primul servit`,
dar implementarile lor sunt foarte diferite.

Diferentele intre cele doua implemetari sunt la nivel de performanta...
Clasa LinkedQueue are adaugarea mai lenta (pentru care trebuie sa navigheze pana ultimul element, ca sa adauge un  element nou)
dar metodele `dequeue` si `peek` sunt foarte rapide, pentru nu trebuie sa facem nici o iteratie.

Clasa StackedQueue are toate operatiile mai lente pentru ca la fiecare operatie trebuie sa mutam literele dintr-o stiva in alta.

Ne putem imagina alte implementari, care sa ne ofere alte caracteristici de performanta.


### Tema

Implementati o coada folosind un array

1) Creatii o clasa numita ArrayQueue care implementeaza interfata Queue, care sa contina un camp de tip array de char, initializat cu 10 pozitii, si un int numit size.
2) la metoda isEmpty verificati daca size este 0
3) la metoda enqueue adaugati elementul pe pozitia lui size si incrementati size.
4) la metoda dequeue luati primul element din array (care va fii returnat) si mutati toate elementele din array de pe pozitia i la i - 1, incepand de la i = 1
   dupa care decrementati size.
5) la peek, luati primul element din array.

6) cand dimensiunea cozi depaseste numarul de poziti din coada (la enqueue) inlocuiti-l cu un array mai mare, si copiati tot din array-ul vechi in cel nou.
7) cand dimensuinea cozi mult mai mica decat dimensiunea array-lul (la dequeue), inlocuiti-l cu un array mai mic, si copiati tot din array-ul vechi in cel nou.
