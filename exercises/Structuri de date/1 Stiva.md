# Stiva

Stiva (sau Stack in engleza) este cea mai simpla structura de date, dar totusi sta 
la baza programarii...

Ea este o structura de date `LIFO` (Last In First Out), sau
Ultimul Venit Primul Servit

Altfel spus, imagineazati ca pui 10 farfurii una peste alta, ca sa le speli.
Cand te apuci de spalat, incepi cu cea mai de sus, adica cu ultima pe care ai pus-o in stiva de farurii.

### Exemplu

Sa zice ca avem literele `a`, `b`, `c` si `d` si stiva `s`


```
literele: a, b, c, d;

  |   |
s=|   |
  |   |
  |___|

```

dupa ce aduagam litera `a` in stiva, ea arata asa:

```
literele: b, c, d;

  |   |
  |   |
s=|   |
  | a |
  |___|

```

dupa ce adaugam `b` in stiva, ea arata asa:


```
literele: c, d;

  |   |
  |   |
s=| b |
  | a |
  |___|

```
Dupa ce adugam `c` si, pe urma, `d`, ea arata asa:

```

  | d |
  | c |
s=| b |
  | a |
  |___|

```


Cand vrem sa scoate din stiva v-om stoate din ea in ordine inversa adaugarii.

```
          | d |                                  |   |
          | c |                                  | c |
Initial s=| b |   daca scoate cv va arata asa s =| b |
          | a |                                  | a | 
          |___|                                  |___|
```

Daca mai scoatem din stiva vom obtine pe rand, `c`, `b` si `a`.


### API 

Putem definii urmatoarele operatii de baza pe o stiva.

```
  void push(e);   -> Introduce un element nou in stiva
  E pop();        -> Scoate cel mai de sus element din stiva si da eroare daca stiva ii goala.
  E peek();       -> Ne da cel mai de sus element din stiva, dar nu il scoate si da erroare daca stiva ii goala
  bool isEmpty(); -> Ne spune daca stiva este goala sau nu...
```

### O posibila implementare.

Exista mai multe variante in care putem implementa o stiva.
In exemplu de mai jos vom implementa o stiva folosind o serie de noduri
in care fiecare nod va contine o valoare si o referinta la nodul urmator din lant...

In primul rand, putem defini o interfata care sa reprezinte API-ul stivei noastre.

```
  public interface Stack {
    void push(char e);
    char pop();
    char peek();
    boolean isEmpty();
  }
```

In continuare vom defini o clasa care implementeaza interfata Stack 
folosind o inlanturie de noduri.


```
  public class LinkedStack implementes Stack {
    private Node top;
    
    @Override
    public void push(char c){
      Node newTop = new Node();
      newTop.value = c;
      newTop.next = this.top;
      this.top = newTop;
    }

    @Override
    public char pop(){
      char value = top.value;
      top = top.next;
      return value;
    }
    
    @Override
    public char peek(){
      return top.value;
    }
    
    @Override
    public boolean isEmpty(){
      return top === null;
    }
  }
```

Observam in implementarea de mai sus clasa Node, pe care inca nu am defint-o.

Daca studiem codul observam ca, clasa Node, nu este nimic mai mult decat o simpla cutie pentru doua valori.
Ea se poate definii in felul urmator:

```
  public class Node {
     char value;
     Node next; 
  }
```

Putem observa ca, clasa Node, este definita in functie de ea insusi.
Adica un Node este `o valoare` si o referinta la `urmatorul node` din lant.

Din cauza acestui lant, denumim aceata implementare a unui `Stack` un `LinkedStack`.


### Detalii de implementare


Putem observa ca stiva noastra este contine intern doar 1 `Node` 
numit `top`.

Acest camp (`top`) reprezinta cel mai de sus element din stiva.
El stie ce valoare are, si ce valoare urmeaza dupa el (prin referinta `next` din clasa `Node`)

#### Metoda isEmpty

Stii ca, inainte sa punem ceva in stiva, valoarea lui `top` va fi `null`.
Astfel putem defini `isEmpty` ca o simpla verificare de `null` a campului `top`.
Daca `top` ii null, stiva ii goala, daca nu ii `null`, stiva nu-i goala...


#### Metoda peek.

Observam ca tot ce trebuie sa facem pentru a implementa metoda peek este 
sa extragem valoarea lui `top`.

Daca `top` este `null` apelul va rezulta intr-o exceptie. 
(exact ce trebuie, conform definitii metodei peek)

#### Metoda pop

Metoda `pop` este similara in definitie, si concept cu metoda `peek`,
Deci in primul rand extragem valoare lui `top` (aceasta valoare v-a fii rezultatul apelului)
Dupa care trebuie sa eliminam cel mai de sus element din stiva.
Putem face asta suprascriind campul `top` cu valoarea urmatoare din `top` 
(adica `top` ia valoarea lui `top.next`)

Din nou observam ca, daca valoarea campului `top` este `null`
apelul functiei va rezulta intr-o exceptie, din nou conform cu definitia metodei `pop`...

#### Metoda push

Daca metoda `pop` suprascria valoarea lui `top` cu urmatorul Node, astfel eleminand
cel mai sus Node din lant.

Metoda `push` va suprascrie valoarea lui `top` cu un Node nou, a cariu `next` este `top-ul curent`...



### Utilizare

Putem folosii o stiva pentru a rezolva o serie de problem.
Una des intalnita (char si in editor-ul de text, pe care il folosim) este 
problema parantezelor balansate.

### Exercitii 

#### Paranteze balansate

Determinatii daca urmatoarele secvente de paranteze sunt balansate
(balansate == toate parantezele deschise au corespondent inchis)

Secvente:
```
  String s1 = "((()))"; //balansat (3 deschise, 3 inchise)
  String s2 = "(()())"; //balansat (parantezele din exterior sunt corecte la fel si cele din interior.
  String s3 = "((())";  // nebalansat ( se deschid 3, dar se inchid doar 2)
  String s4 = "(()))";  // nebalansat ( se deschid 2 dar se inchid 3)
```

```

  public static boolean isOpenParent(char c){
    return c == '(';
  }

  public static boolean isCloseParen(char c){
    return c == ')';
  }
  
  public static boolean  parensMatch(char open, char close){
    return open == '(' && close == ')';
  }  

  public static boolean isBalanced(String str){
      Stack s = new LinkedStack();
      for(int i = 0; i < str.length(); i++){
        char c = str.charAt(i);
        if(isOpenParent(c)){
           s.push(c);
        }else if(isCloseParen(c)){
          if(s.isEmpty() || !parensMatch(s.pop(), c)){
            return false;
          }
        }
      }
      return s.isEmpty();
  }

```

### Tema

Extindeti functiilea isOpenParen, isCloseParen si parensMatch astfel ca metoda isBalanced 
sa suporte mai multe feluri de parenteze (adica [] si {}),

Parentezele trebuie sa se inchida correct (ca la matematica) de exemplu:
```
  String s1 = "(([]))"; // ok
  String s2 = "(([)])"; // nu-i ok
  String s3 = "({[()]})"; // ok
  String s4 = "{()[}]"; // nu-i ok
```
