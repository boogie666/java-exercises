# Lista


### Exemplu


### API 

Putem definii urmatoarele operatii de baza a unei liste.

```
  void add(e);   -> Adauga un element nou in lista (la coada)
  E remove(i);   -> Sterget al i-lea element din lista, sau arunca o exceptie in cazul in care 'i' se afla in afara intervalului [0, lungimea listei).
  E get(i);      -> returneaza al i-lea element din lista, sau arunca o exceptie in cazul in care 'i' se afla in afara intervalului [0, lunginea listel).
  int size();    -> ne spune cat de multe elemente sunt in lista.
```

### 2 posibile implementari.

In primul rand ne putem definii o interfata numita List
care reprezinta API-ul aferent unei liste...

```
  public interface List {
    void add(char e);
    char remove(int i);
    char get(int i);
    int size();
  }
```


#### Lista simpla inlantuita (LinkedList)


Pentru implentarea unei liste inlantuite ne vom folosii de de clasa Node (de la Stiva)

```

public class LinkedList implements List {
  private Node first;

  @Override
  public void add(char c) {
    if (first == null) {
      first = new Node();
      first.value = c;
      return;
    }
    Node last = first;
    while (last.next != null) {
      last = last.next;
    }

    Node newLast = new Node();
    newLast.value = c;
    last.next = newLast;
  }

  @Override
  public char remove(int i) {
    int index = i;
    Node n = first;
    Node prev = null;
    while (i > 0 && n != null) {
      prev = n;
      n = n.next;
      i--;
    }

    if (index < 0 || n == null) {
      throw new RuntimeException("Index out of bounds " + index);
    }
    if (index == 0) {
      char c = first.value;
      first = first.next;
      return c;
    }
    
    char v = n.value;
    prev.next = n.next;
    return v;
  }

  @Override
  public char get(int i) {
    int index = i;
    Node n = first;
    while (i > 0 && n != null) {
      n = n.next;
      i--;
    }
    if (n == null && index < 0) {
      throw new RuntimeException("Index out of range " + index);
    }
    return n.value;
  }

  @Override
  public int size() {
    int size = 0;
    Node n = first;
    while (n != null) {
      n = n.next;
      size += 1;
    }
    return size;
  }
}


```

Daca comparam clasele LinkedQueue si LinkedList, putem vedea ca
ele sunt, mai mult sau mai putin, echivalente.

`LinkedList#add` si `LinkedQueue#enqueue` sunt identice.
`LinkedList#remove(i)` si `LinkedQueue#dequeue` sunt foarte identice pentru remove(0)
`LinkedList#get(i)` si `LinkedQueue#peek` sunt foarte identice pentru get(0)
`LinkedList#size` si `LinkedQueue#isEmpty` sunt complementare (adica isEmpty poate fi implementat in functie de size)

#### Lista folosind un Array (ArrayList)


```

public class ArrayList implements List{

	private char[] array;
	private int size = 0;
	
	
	public ArrayList() {
		this.array = new char[8];
	}
	
	@Override
	public void add(char e) {
		if(size >= array.length) {
			resize(array.length * 2);
		}
		this.array[size++] = e;
	}
	
	@Override
	public char remove(int i) {
		char c = get(i);
		size--;
		for(; i < size; i++) {
			array[i] = array[i + 1];
		}
		
		if(size <= array.length / 3) {
			resize(array.length / 2);
		}
		
		return c;
	}
	
	private void resize(int newLength) {
		char[] newArray = new char[newLength];
		int length = Math.min(array.length, newArray.length);
		for(int i = 0; i < length; i++) {
			newArray[i] = array[i];
		}
		
		this.array = newArray;
	}
	
	@Override
	public char get(int i) {
		if(i < 0 && i >= size) {
			throw new IndexOutOfBoundsException("Index out of bounds: " + i);
		}
		return array[i];
	}

	@Override
	public int size() {
		return size;
	}

} 

```


Pentru un ArrayList vedem ca folosim un Array de litere ca sa stocam 
lista. 

Dar, in java, ca in multe alte limbaje, un array nu creste dinamic, ci are capacitate fixa.
Pentru a elimina aceasta limitare trebuie noi sa redimensionam dinamic array-ul cand este necesar.


Cand creeam un ArrayList, creeam si array-ul din spate, initial cu 8 pozitii (numar ales arbitrar)

Cand adaugam in lista, verificam daca mai avem loc in array.

Daca avem loc, stocam elementul adaugat in array la pozitia urmatoare (conform size-ul si incerementam size-ul)
Daca nu avem loc, redimensionam array-ul la  dublu fata de cat este momentan
(la al 9-lea element array-ul va avea 16  pozitii, la al 17-lea element, va avea 32, etc.)
dupa care adaugam in array la pozitia urmatoare, ca inaite.

Cand scoatem un element din array.

in primul rand decrementam size-ul array-ul (pentru ca am scos un element)
pe urma, pentru mutam toate elemente de index-ul de unde am scos, pana la size-ul nou, cu unu mai in fata.


ca sa extragem al i-lea element din lista, tot ce trebuie sa facem este sa il scoatem din array.
(cu o verificare extra ca i-ul dorit este in intervalul [0, size), nu intervalul [0, array.length))



### Tema


1) Faceti clasa LinkedList sa implementeze interfata Queue
2) Creati clasa ArrayListQueue care sa implementeze interfata Queue.
   In implementare folositi un ArrayList ca structura ajutatoare in implementarea cozii
