# Space Shooter - Part 1

### Setup initial.

In `libGDX` clasa Main este responsabila doar pentru configurarea initiala a jocului.

```java

  public class Main {
    public static void main (String[] args) {
      LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
      config.width = 800;
      config.height = 600;
      config.title = "Space shooter";
      new LwjglApplication(new SpaceShooter(), config);
    }
  }

```

Dupa cum putem observa in codul de mai sus, programul incepe cu 
creea unui obiect `config`.

Folosind acest obiect putem seta proprietati 'globale' ale jocului,
cum ar fii titlul ecranului si cat de mare este ecranul in pixeli (800x600 in cazul acesta)

Dupa creea config-ului, creeam un `LwjglApplication` care foloseste acest `config` si
un nou obiect de tipul `SpaceShooter`.

Clasa `SpaceShooter` este clasa in care ne v-om implementa jocul nostru.


### Clasa SpaceShooter

Dupa cum putem sa obervam in [documentatia libGDX](https://github.com/libgdx/libgdx/wiki/The-life-cycle)
clasa in care scriem jocul (clasa `SpaceShooter` in cazul nostru) trebuie sa implementeze 
interfata `ApplicationListener` din libGDX.


```java
import com.badlogic.gdx.ApplicationListener;

public class SpaceShooter implements ApplicationListener {

	@Override
	public void create() {
	}

  @Override
	public void render() {
	}

  @Override
	public void dispose() {
	}

	@Override
	public void resize(int width, int height) {
	}

  @Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}

```

Din numele metodelor, putem deduce unele aspecte ale modului in care functioneaza `libgdx`
Aceste metode for fi apelate de catre biblioteca la anumite momente.

  * metoda `create` - este apelata doar odata, la initiazarea jocului, aici v-om initiaza jocul
  * metoda `render` - este apelata de 60 de ori pe secunda, aici v-om avea logica pentru a desena jocul si logica pentru a modifca jocul
  * metoda `dispose` - este apelata la final, pentru a face curatenie si pentru a elibera resursele jocului
  * metodele `pause` si `resume` - sunt apelate cand punem jocul in pauza si revenim. (sunt relevante doar pentru jocuri mobile)
  * metoda `resize` - este apelata cand dimensiunile ecranului se modifica.

Putem gasii mai multe detalii [aici](https://github.com/libgdx/libgdx/wiki/The-life-cycle)


### Clasa Abstracta Entity


Pentru a face orice in joc, avem nevoie de chestii care fac ceva in joc...
Aceste 'chestii' pot fii Jucatorul, Inamicii, Gloantele, Power-up-uri etc...

Pentru a impementa aceste entitati (uitandune la UML) putem vedea o serie de prorietati comune
care toate aceste entitati le au...

Stii ca orice entitate are o pozitie in spatiu (coordonate x,y) o textura (o poza)
metode de update si render (pentru a schimba starea si pentru se desena in ecran)
si o metoda dispose (pentru a elibera resurse, cand entitatea moare)...

Pentru asta putem creea clasa abstracta `Entity`...

De ce abstracta? pentru ca fiecare entity va trebuie sa faca altceva, in functie de ce ii...

Clasa Player va fi controlata de jucator, iar pozitia x/y se va modifica in functie de pozitia mouse-ului.
Clasa Enemy va fi controlata de o "inteligenta artificiala" care va face inamicul sa mearga in jos.
Clasa Bullet va fi controlata tot de o "inteligenta artificiala", dar va merge in sus.



Din cauza asta, clasa Entity, care va fii la baza tutoror entitatilor trebuie sa fie abstracta,
si sa aibe metoda update abstracta.


#### Varianta simpla

Va trebuii sa facem, initial, o varianta simpla a clasei entity.
Initial va contine doar coordonatele x/y, ambele `float`,
o textura, metodele `render`, `dispose` si `update` (abstracta)...


in metoda `render` v-om definii logica de desenare in felul urmator:

```
batch.draw(this.texture, this.x - this.texture.getWidth() / 2, this.y - this.texture.getHeight() / 2);
```

facem asta pentru a desena textura din centru.


in metoda `dispose` v-om distruge textura.

```
this.texture.dispose();
```

metoda `update` va ramane abstracta, si va fii implementa in sub-clase.


#### Alte entitati...

Conform diagrame UML (din part 0) v-om creea alte 3 clase...
Un `Player`, un `Enemy` si un `Bullet`

Clasa `Player` va avea un construtor in care v-om initializa textura cu 'nava jucatorului'
iar in metoda `update` (mostenita din clasa parinte `Entity`) v-om face

```
  this.x = Gdx.input.getX();
  this.y = Gdx.graphics.getHeight() - Gdx.input.getY(); // trebuie sa inversam y...
```

Clasa `Enemy` va fi similara dar in textura va fi initializata cu alta poza...
pe langa asta constructorul clasei trebuie a primeasca coordonata `x` ca parametru,
iar `y` va fii setat pe ceva valoare deasupra de marginea ecranului.

```
  public Enemy(float x){
    this.x = x;
    this.y = 800;
    this.texture = new Texture("enemy.png");
  }
```

iar metoda `update` doar va incrementa 'y' cu o valoare.

```
  this.y -= 10; // 'A.I.' care face inamicul sa vina de sus in jos.
```


Clasa `Bullet`, va fii foarte similara cu clasa `Enemy` dar ambele coordonate
trebuie pasate prin constructor, iar metoda update va merge invers (adica va aduna ceva numar la y, inloc sa il scada)

```
  public Bullet(float x, float y){
    this.x = x;
    this.y = y;
    this.texture = new Texture("bullet.png");
  }
  
  public void update(){
    this.y += 20; // din nou 'A.I', care va face laser-ul sa mearga in sus pe ecran
  }

```

### Un mic test...

Daca ne intracem in clasa `SpaceShooter` putem sa testam aceste clasa inainte de a continua.

```java
public class SpaceShooter implements ApplicationListener {

	private Entity player;
	private Entity bullet;
	private Entity enemy;
	private SpriteBatch batch;

	@Override
	public void create() {
		this.batch = new SpriteBatch();
		this.player = new Player();
		this.enemy = new Enemy(200);
		this.bullet = new Bullet(100, 100);
	}

	@Override
	public void render() {
		this.player.update();
		this.enemy.update();
		this.bullet.update();

		this.batch.begin();
		this.player.render(batch);
		this.enemy.render(batch);
		this.bullet.render(batch);
		this.batch.end();
	}

	@Override
	public void dispose() {
		this.batch.dispose();
		this.player.dispose();
		this.bullet.dispose();
		this.enemy.dispose();
	}
}
```

Daca rulam jocul acum, observam ca nava jucatorui urmareste mouse-ul, glontul merge in sus
si inamicul in jos.


