# Space Shooter Part 4


Intr-o lume ideala, entitatile noastre nu ar face modifcari in entity manager singure.
si nu ar stii ce se intampla cand se lovesc de alte entitati...

De ce ar fi aia o lume ideala?

Pentru cu cat putem sa avem mai putin cod, cu atat codul este mai usor de inteles
si cu cat adaugam mai multa logica in toate partile, cu atat ii mai greu de inteles.

Hai sa facem lumea ideala...


### Design Patterns again...

Idea de a 'deconecta' sau a `decupla` parti diferite de cod este 
esenta design pattern-ului numit [Observer](https://en.wikipedia.org/wiki/Observer_pattern)
(si a unui pattern derivat din Observer nimit [Publish-Subscribe](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern) )

Practic `Observer pattern`-ul imparte clasele in doua family (legate intre ele printr-o interfata)

Clasa "Observabile" si clase "Observatoare"...

Sau clasa care emit evenimente si clase care reactioneaza la evenimente.

Daca ne uitam la diagrama UML vedem acolo o clasa numita `EventBus`
aceasta clasa are doua metode importante.

Metoda `send` (folosita pentru a trimite eveminente) si metoda `addHandler` (folosita pentru a inregistra o clasa ca consumator de evenimente)...


Mai putem sa observam un lucru interesant in diagrama clasei `EventBus`...
Ea are constructorul privat si in camp static numit `instance` (si o metoda static `getInstance`)

Aceste doua lucruri sunt caracteristice unui alt design pattern nimit [Singleton](https://en.wikipedia.org/wiki/Singleton_pattern)
Acest design pattern ne permite sa facem ca o clasa sa aibe doar o singura instanta, global accesibila pe durata rulari unui program.

In cazul clasei `EventBus` aceste doua design pattern-uri ne ofera exact ce dorim.
Si anume, un mod prin care clase Entity pot publica evenimente, cand se intampla ceva cu ele, 
iar clasa SpaceShooter poate asculta si reactiona la aceste evenimente


### Implementarea EventBus-ului

```

// Fisierul EventBus.java
public class EventBus{
  // Singleton pattern

  private static EventBus instance = new EventBus();
  private EventBus(){} 
  public static EventBus getInstance(){
    return instance;
  }
  
  // Observer Pattern
  private List<Handler> handlers = new ArrayList<Handler>();

  public void addHandler(Handler h){
    this.handlers.add(h);
  }

  public void send(Event e, Entity sender){
    for(int i = 0; i < this.handlers.size(); i++){
      this.handlers.get(i).handle(e, sender);
    }
  }
}

// Fisierul Handler.java
public interface Handler {
   void handle(Event e, Entity sender);
}

//Fisierul Event.java

public enum Event{
  PLAYER_FIRE,
  ENEMY_HIT_PLAYER,
  PLAYER_HIT_ENEMY,
  BULLET_HIT_ENEMY,
  ENEMY_HIT_BULLET,
  REMOVE_ENTITY
  // v-om mai adauga tipuri de evenimente aici
}

```

Dupa cum observam, `EventBus`-ul este impartit in 2 parti. 
Partea in care il facem singleton si parte in care il facem Observabil.

Singleton are doar 5 linii de cod.
In prima definim un camp static de acelasi tip ca si clasa pe care dorim sa facem singlon.
Aceata va fii instanta unica.
Pe a doua setam constructorul ca fiind privat pentru a prevenii creeare de alte instante.
Dupa care ne trebuie un mod de a accesa instanta unica creeata.

Observer pattern-ul este un pic mai complicat.

Observam ( heh :) ) ca avem nevoie de inca un fisier pentrul al implementat complet (interfata Handler).



### Cum folosim asta???


#### Entitati

Incepem cu clasa Player.

In clasa Player avem doua lucruri care putem sa redefinim ca evenimente.
Cand jucatorul apasa pe tragaci, si cand player-ul se loveste de un inamic.

Astfel metoda `update` din clasa Player se va schimba asa:

```

public void update(){
  this.x = Gdx.input.getX();
  this.y = Gdx.graphics.getHeight() - Gdx.input.getY();
  if(Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)){
    EventBus.getInstance().send(Event.PLAYER_FIRE, this); // evenimentul "Am tras cu laser"
  } 
  
  List<Entity> allEntities = this.entityManager.getEntities();
  for(int i = 0; i < allEntities.size(); i++){
    Entity other = allEntities.get(i);
    if(other instanceof Enemy){ // daca entitatea pe care o verifica este de tipul Enemy
      if(this.collidesWith(other)){ // si daca ne lovim 
        EventBus.getInstance().send(Event.PLAYER_HIT_ENEMY, this); // o pereche de eveminente ("eu am lovit un inamic" si "inamicul m-a lovit pe mine")
        EventBus.getInstance().send(Event.ENEMY_HIT_PLAYER, other);
      }
    }
  } 
}

```

Metoda `update` din clasa Bullet se va schimba astfel:
Observam aceasi idee generala, dar alt set de evenimente...

```
public void update(){
  this.y += 20;
  if(this.y > 800){
    EventBus.getInstance().send(Event.REMOVE_ENTITY, this);
  }

  List<Entity> allEntities = this.entityManager.getEntities();
  for(int i = 0; i < allEntities.size(); i++){
    Entity other = allEntities.get(i);
    if(other instanceof Enemy){ 
      if(this.collidesWith(other)){ 
        EventBus.getInstance().send(Event.BULLET_HIT_ENEMY, this); // o pereche de eveminente ("eu am lovit un inamic" si "inamicul m-a lovit pe mine")
        EventBus.getInstance().send(Event.ENEMY_HIT_BULLET, other);
      }
    }
  } 
  
}

```

Iar `update` din Enemy devine:

```
public void update(){
  this.y -= 10;
  if(this.y < 100){
    EventBus.getInstance().send(Event.REMOVE_ENTITY, this);
  }
}
```


#### Handler-ul

Pentru moment clasa SpaceShooter va fii singurul handler de evenimente...


```
public class SpaceShooter implements ApplicationListener, Handler{
  ...
  
  @Override
  public void create(){
    ...
    EventBus.getInstance().addHandler(this);
  }  
  ...
  @Override
  public void handle(Event e, Entity sender){
    if(e == Event.REMOVE_ENTITY){
      this.entityManager.remove(sender);
    }
    if(e == Event.PLAYER_FIRE){
      this.entityManager.createBullet(sender.getX(), sender.getY()); // atentie metodele getX si getY, nu exista si vor trebuii create...
      // mai putem sa adaugam sunet maybe?
    }
    if(e == Event.ENEMY_HIT_PLAYER){
      this.entityManager.remove(sender);  
    }
    if(e == Event.PLAYER_HIT_ENEMY){
      // aici o sa fie codul pentru scazut de viata
    }
    if(e == Event.BULLET_HIT_ENEMY){
      this.entityManager.remove(sender);
      // mai putem sa adaugam o expozie maybe???
      // si crescut scor
    }
    if(e == Event.ENEMY_HIT_BULLET){
      this.entityManager.remove(sender);
    }

  }
}
``` 

