# Space Shooter Bonus


### Scrolling Backgrounds


Putem adauga un fundal care pare sa mearga la infinit in felul urmator:

Daca avem o poza a carai parte de sus este la fel ca parte de jos 
(asta se numeste 'tiling image' sau `tileable image`) putem crea efectul
unui fundal in eterna miscare.

Astfel putem sa dam senzatia de miscare in joc.


Putem implementa o varianta simpla a acestui effect in felul urmator


```java
public class SpaceShooter{
  ...
  private Texture background;
  private float scrollY;

  @Override
  public void create(){
    ...

    this.background = new Texture("tileing_background.png");
    this.scrollY = 0;
  }

  @Override
  public void render(){
    ...
    scrollY -= 20; // viteza cu care vrem sa se miste fundalul;
    if(scrollY < -this.background.getHeight()){ //cand am trecut de prima poza
      scrollY = 0; //resetam pozele astfel incat prima poza sa ia locul pozei 2 si poza 2 sa fie deasupra din nou.
    }
    
    ...

    this.batch.begin();
    this.batch.draw(this.background, 0, scrollY + background.getHeight()); //desenam poza 2
    this.batch.draw(this.background, 0, scrollY); // deseman poza 1
    ...
    this.batch.end();
  }
}

```

Bineinteles, putem refactoriza codul acesta folosind o clasa.
Putem crea o clasa numita `ScrollingBackground` care sa incapsuleze logica aceasta.


```

public class ScrollingBackground{
  // atentie, trebuie sa punem trei campuri aici...
  // viteza, poza, si pozitia pe Y...
  public ScrollingBackground(float speed, Texture background){
    this.speed = speed; 
    this.background = background;
  }

  public void update(){
    // va descrucati aici
    // logica este foarte simpla
    // in codul de mai sus am descris cum se modifica variabila scrollY in functie 'viteza'
    // si de inaltimea pozei folosite ca background.
    // in cazul de mai sus viteza a fost 20, acum este campul `speed`
  }

  public void render(SpriteBatch sb){
    // va descurcati aici...
    // practic logica de desenare care am descris-o mai sus
    // poate fi mutata aici
  }

  
}

```

Dupa ce implementam clasa `ScrollingBackground` putem sa adaugam mai multe poze (ideal una fara transparenta si restu cu ceva transparenta)
suprapuse si la viteze diferet de scroll, creea efectul mulit [parallax scrolling](https://en.wikipedia.org/wiki/Parallax_scrolling)


```
  public class SpaceShooter ... {
      ...
      // bine inteles ca variabiele scrollY si textura pentru 'background' 
      // nu mai se afla direct in clasa aceasta
      // pentru ca tocmai ce le-am mutat in clasa scrolling background...
      // in locul acelor campuri, putem folosi direct intante ale clasei
      // nou create
      ScrollingBackground layer1;
      ScrollingBackground layer2;
      ScrollingBackground layer3;

      @Override
      public void create(){
        ...
        layer1 = new ScrollingBackground(5, new Texture("layer1.png"));
        layer2 = new ScrollingBackground(10, new Texture("layer2.png"));
        layer3 = new ScrollingBackground(15, new Texture("layer3.png"));
      }

      @Override
      public void render(){
        ...
        layer1.update(); 
        layer2.update(); 
        layer3.update(); 

        this.batch.begin();
        layer1.render(this.batch);
        layer2.render(this.batch);
        layer3.render(this.batch);
        ...
        this.batch.end();
      }
  
  }

```


Acest effect poate fi vazut in multe jocuri 2d pentru a da o senzatie de profunzime la joc.
In unele cazuri se pot folosi cateva layere pentru fundal si cateva layere pentru "spatiul dintre ecran si personajul jucatorului"
(asa numitul `foreground`) pentru a crea un efect de profunzime si mai intens...

Acest effect este folosit cu foarte mare succes in jocul `Hollow Knight` (dupa parerea mea)

O expliata mai in detaliu a acestui effect o gasiti [aici](https://www.youtube.com/watch?v=z9tBce8eFqE)


### Animatii


O animatie nu este nimic mai mult decat o succesiune rapida de imagini.
LibGDX ne ofera mai multe variante pentru a obtine acelasi rezultat.


Avand la dispozitie aceasta animatie pentru o explosie

![explosion.png](/images/games/explosion.png)


Putem observa ca o singura poza contine mai multe cadre (sau `frame`-uri) de animatie.
5x5 cadre, adica 25 de cadre, fiecare de latime si inaltime egala cu 64.

Avem doua variante la dispozie,

1) creem din acest `spritesheet` 25 de `sprite`-uri independente (folosind un program de editare de imagini, poate chair `photoshop-ul saracului`)
2) impartim textura procedural in 25 de cadre, folosind utilitatile din LibGDX.


In clasa SpaceShooter putem adauga un nou camp de timp-ul array de `TextureRegion`, clasa care ne permite sa selectam doar o parte dintr-o textura data
in felul urmator.


```java
public class SpaceShooter{
  ...
  private Texture[] explosionAnimation;
  private int currentFrame = 0;
  private float frameTime;
  private float timePerFrame = 0.05f;  

  @Override
  public void create(){
    ...
    Texture explosionSprite = new Texture("explosion.png");
    int frameSize = 64;
    TextureRegion[][] frames = TextureRegion.split(explosionSprite, frameSize, frameSize);
    this.explosionAnimation = new TextureRegion[frames.length * frames[i].length];
    int k = 0;
    for(int i = 0; i < frames.length; i++){
      for(int j = 0; j < frames[i].length; j++){
        this.explosionAnimation[k] = frames[i][j];
        k++;
      }
    }
  }

  ...
    
  @Override
  public void render(){
    ...
     
    frameTime += Gdx.graphics.getDeltaTime();
    if(frameTime > timePerFrame){ 
      frameTime = 0;
      currentFrame++;
      if(currentFrame >= this.explosionAnimation.length){
        currentFrame = 0;
      }
    }
    ...

    this.batch.begin();
    this.batch.draw(this.explosionAnimation[currentFrame], 100, 100);
    ...
    this.batch.end();
  }
}

```


Similar `ScrollingBackground` putem extrage o clasa care sa incapsuleze ideea de `Animation`

```java

public class Animation{

  float frameTime;
  float timerPerFrame;
  TextureRegion[] animation;
  int currentFrame;
  boolean done;
  float positionX;
  float positionY;

  public Animation(float timerPerFrame, int frameSize, Texture spriteSheet){
    this.currentFrame = 0;
    this.timePerFrame = timerPerFrame;
    this.frameTime = 0; 
    this.done = false;
    this.animation = ... // aceasi initializare ca mai sus, dar array-ul se cheama acum animation nu explosionAnimation
  }

  public void update(){
    if(done){//daca am terminat animatia, ne oprim
      return;
    }
    frameTime += Gdx.graphics.getDeltaTime();
    if(frameTime > timePerFrame){ 
      frameTime = 0;
      currentFrame++;
      if(currentFrame >= this.explosionAnimation.length - 1){ // cand am ajuns la ultimul cadru ne oprim
        done = true;
      }
    }
  }

  public void render(SpriteBatch sb, float x, float y){
    sb.draw(this.frames[currentFrame], x, y);
  }

  public boolean isDone(){
    return this.done;
  }
}

```

Dupa ce avem clasa Animation putem creea o noua entitate care sa reprezinte explozia.

```java

public class Explosion extends Entity {
  Animation animation;
  Texture explosionSprite;
  public Explosion(float x, float y){
    this.x = x;
    this.y = y;
    this.explosionSprite = new Texture("explosion.png");
    this.animation = new Animation(0.05f, 64, this.explosionSprite);
    this.bounds = new Rectangle(); // entity are nevoie de `bounds`, dar in cazul acesta nu trebuie sa facem nimic cu el...
                                   // pentru ca nimic nu se loveste o explozie...
  }
  
  @Override
  public void update(){
    animation.update();
    if(animation.isDone()){
      this.entityManager.remove(this); // ne stergem cand am terminat lucrul
    }
  }
  
  @Override
  public void render(SpriteBatch sb){ // trebuie sa suprascriem metoda render din `Entity` pentru a desena animatia.
    animation.render(sb, this.x, this.y);
  }

  @Override
  public void dispose(){
    this.explosionSprite.dispose();
  }
}
 
```

Acum putem sa folosim clasa `Explosion` in joc, intr-ul mod simular cu clasa `Bullet`.
Dar in loc sa creeam o instanta cand jucatorul apasa pe tragaci,
creeam o instanta de explosion cand un inamic este distrus (pe pozitia x/y a inamicului)


### Sunet


Exista doua feluri de `sunete` in libgdx.

1) `Sound` - folosit pentru clip-uri scurte (gen explozii, sunet de laser cand tragi etc...)
2) `Music` - folosit pentru muzica de fundal, sau clipuri lungi. (gen coloana sonora a jocului, sau muzica ambientala etc...)

Ambele sunt aproximativ la fel de folosit.

Pentru `Sound` putem folosi urmatoarele:

```
  Sound s = Gdx.audio.newSound("explosion.wav"); // la initializare

  s.play(); // la folosire.
```


Iar pentru `Music` putem folosi:

``` 
  Music m = Gdx.audio.newMusic("background_theme.mp3"); // la initializare
  
  m.setLooping(true); // daca vrem sa ca dupa ce se termina melodia sa inceapa de la inceput.
  m.play(); // take a wild guess what this one does...
  m.pause();// take another wild guess here...
  m.stop(); // citeste cu o linie mai sus...
  

```

In ambele cazuri este bine sa apela metoda `dispose` cand nu mai avem nevoie de ele...
(de obicei la inchiderea jocului)


Puteti citi mai multe despre `Sound` [aici](https://github.com/libgdx/libgdx/wiki/Sound-effects)
si despre `Music` [aici](https://github.com/libgdx/libgdx/wiki/Streaming-music)

Exista multe alte lucruri care se pot realiza cu sunetul in jocuri.

un lucru interesant ar fi `SpacializedSound` (sau sunet in spatiu 3d/2d)

LibGdx nu ofera posibilitatea directa pentru asta dar, ea se poate adauga separat...
(Google it guys...)



### O alta abordare pentru Game Development.

In acest proiect am luat o anume abordare pentru a creea acest joc simplu,
dar nu este singurul mod in care putem scrie jocuri...


Gasiti [aici](https://gitlab.com/boogie666/space-shooter) o alta implementare a acestui joc 
folosit o alta aritectura numita `Entity-Component Architecture` (ii aceeasi arhitectura folosita de Unity)

Puteti studia si modifica codul cum doriti...

Pentru al importa in eclipse folositi `File > Import... > Maven > Existing Maven Projects ...` dupa care navigati unde ati descarcat jocul.


