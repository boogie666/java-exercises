# LibGDX


### Un pic de context

libGDX este o biblioteca "free si open source" creata pentru a face jocuri in limbajul de programe java.
A aparut pe 20 aprilie 2014 (acum 5 ani de zile).

Ofera posibilatea de a face jocuri pentru Windows, Linux, Mac OSX, Android, iOS, blackberry si browsere moderne.

libGDX este construit cu filozofia java in minte "Write once, run anyware".

Principala parte a bilioteci este biblioteca numita "Light Weight Java Games Library" (sau LWJGL),
biblioteca care sa la baza jocului Minecraft.

Biblioteca permite dezvoltarea atat de jocuri 2D cat si 3D (sau chiar si o combinatie de ele).

libGDX ofera o seria de plugin-uri pentru a lucra cu diverse alte biblioteci cunoscute in industrie,
cu ar fi Box2D (folosit pentru a simula fizica 2d realista in jocuri precum Angry Birds) sau
Ashley (un framework Enity-Component-System, foarte silimar cu cel gasit in Unity Game Engine),
precum si alte plugin-uri pentru inteligenta artificiala la jocuri si multe altele.


Mai multe detalii se pot gasi [aici](https://github.com/libgdx/libgdx/wiki)


### Setup initial

La baza unui proiect libGDX este interfata `com.badlogic.gdx.ApplicationListener`

Aceata interfata trebuie implementata in clasa principala a oricarui proiect libGDX si ofera o serie de metode
de `lifecycle` pentru joc.

`lifecycle`-ul este o serie de metode (abstracte, adica implementate in fiecare joc altfel), care se apleaza
de catre biblioteca `libGDX` la anumite momente cheie.

```java
public class Game implements ApplicationListener{

	@Override
	public void create() {
    //se apleaza la initializarea jocului
	}

	@Override
	public void dispose() {
    //se apeleaza la inchiderea jocului
	}

	@Override
	public void pause() {
    // se apeleaza cand jocul se pune pe 'pauza'
	}

  @Override
	public void resume() {
    // se apeleaza cand jocul se revine din 'pauza'
	}

	@Override
	public void resize(int width, int height) {
    // se apeaseaza cand ecranul se redimensioneaza
	}

  @Override
	public void render() {
    // se apeleaza de aprox. 60 de ori pe secunda
	}
}
```

Principla metoda este `render`. Aceasta metoda este apelata de ~60 ori pe secunda
si este responsabila pentru executarea de logica si de desenare a jocului.


Dar `Game` nu are un `main`, deci nu poate fi rulat de java.

Pentru a avea un punct de intrare in program debuie sa definim o clasa cu `main`.


```java

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {

	public static void main(String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 600;
		config.title = "Un Joc";
		
		new LwjglApplication(new Game(), config);
	}

}

```


Daca rulam acest program, o sa vedem o fereastra de dimensiune 800x600 cu titlul "Un Joc",
dar ecarnul este gol (si posibil cu artefacte de desenare)


### Ceva pe ecran

Pentru a desena ceva pe ecran trebuie, in primul rand, sa stergem ce era acolo inainte.

```
public class Game implements ApplicationListener{

	@Override
	public void create() {
    Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1); // va seta ce culoare sa fie folosita ca fundal.
    // culorile sunt exprimate in rgba unde 0 este negru si 1 este alb.
    // astfel r = 0.3f, g = 0.3f, b = 0.3f si a = 1 este o nuanta de gri, relative inchis (ca ii mai aproape de 0 decat de 1)
	}

  @Override
	public void render() {
    Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT); // acest apel va sterge tot ce ii pe ecran.
	}

  //... restul metodelor sunt omise pentru a pastra codul scurt.
}
```

Daca rulam clasa Main acum, observam ca ecranul are coloarea gri (si orice artefact de desenare a disparut)

#### Poze simple si statice.

Putem desena `o poza` pe ecran foarte simplu folosind 2 clase din `libGDX`, 
clasa `com.badlogic.gdx.graphics.Texture` si clasa `com.badlogic.gdx.graphics.g2d.SpriteBatch`

Pentru asta v-om declara doua campuri in clasa `Game`, o textura si un sprite batch,
v-om initializa aceste doua campuri in metoda `create`,
le v-om folosin in metoda render, si le v-om distruge in metoda `dispose`

```java

public class Game implements ApplicationListener{
	
  // definim doua campuri, o textura si o poza
	private Texture oPoza; 
	private SpriteBatch batch;
	
	@Override
	public void create() {
		Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
    // le initializam
		this.oPoza = new Texture("o poza.png"); // similar cu biblioteca `pixels` "o poza.png" trebuie sa fie in folder-ul jocului
		this.batch = new SpriteBatch(); 
	}

	@Override
	public void dispose() {
    // le distrugem folosind metoda `dispose` de pe fiecare din campuri
		this.oPoza.dispose();
		this.batch.dispose();
	}

	@Override
	public void render() {
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		this.batch.begin(); // anuntam sprite batch-ul ca o sa incepem sa desenam

		this.batch.draw(this.oPoza, 100, 100); // desenam 'oPoza' la coordonatele x=100, y=100

		this.batch.end(); // anuntam sprite batch-ul ca am terminat treaba, astfel batch-ul poate trimite textura catre placa grafica
    // asta se face de 60 de ori pe secunda
	}

  // restul metodelor sunt omise pentru a pastra codul scurt
}

```

### Animatie simpla

Orice animatie este, la nivel practic, doar o serie de poze, care se schimba foarte repede.

Putem face poza nostra sa 'pice' daca ii modificam coordonata 'y' la care o desenam.
Pentru a face asta trebuie sa:
  * definim un camp nou, de tip float, numit 'y';
  * sa determina noua valoare a lui 'y'
  * desenam poza la acest 'y'.
  

Sau in cod:

```java

public class Game implements ApplicationListener{
	
	private Texture oPoza; 
	private SpriteBatch batch;
  private float y;
	
	@Override
	public void create() {
		Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
		this.oPoza = new Texture("o poza.png");
		this.batch = new SpriteBatch(); 
    this.y = 100;
	}

	@Override
	public void dispose() {
		this.oPoza.dispose();
		this.batch.dispose();
	}

	@Override
	public void render() {
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
    y += 1; // la fiecare frame (de 60 de ori pe secunda), incrementam y cu 1;

		this.batch.begin(); 
		this.batch.draw(this.oPoza, 100, y); 
		this.batch.end();
	}

  // restul metodelor sunt omise pentru a pastra codul scurt
}

```

Daca rulam acum codul, observam ca poza 'pica'... si iese din ecran.


### Alte Concepte in LibGDX

Exita a gama foarte larga de concepte in biblioteca libGDX pe care nu o sa le exploram 
ca de exemplu:

  * Camera Control - sau cum anume miscam 'camera video' virtual prin lume
  * Asset Management - sau cum optimizam resursele grafice/audio/etc... a unui joc
  * Game States - sau cum creeam 'meniuri' sau alte nivele si tranzitionam de la o stare la alta.
  * Simulari de fizica - sau cum facem interactiuni realiste intre obiecte (cu Box2D, sau Bullet pentru 3d)
  * Arhitecturi flexibile pentru jocuri - printre care ECS, Entity-Component, sau altele.
  * si multe alte aspecte ale dezvoltarii de jocuri.

Dar va invit sa 'Google it!'...

Youtube reprezinta o resursa foarte buna pentru invatare.
Cautati 'libGDX game development'




