# Space Shooter


In aceest proiect, v-om creea un joc.

In acest joc, eroul principal este o nava spatiala, care trebuie sa calatoreasa prin spatiu,
catre o destinatie necunoscuta.

Pe drum, erorul nostru, va trebuii sa se apere de nave extraterestre.

In calatoria lui, eroul va avea 3 'clone' (vieti) la dispozitie pentru putea
sa calatoreasca cat mai departe.

Jucatorul va putea sa controleze nava folosind mouse-ul.
Nava v-a urmari mouse-ul, iar la click va trage cu lasere.

Apasand pe tasta `ESC` jucatorul va putea sa puna pauza la joc.

Scorul va fi determinat pe  baza distantei care a parcurs-o
dar si cati inamici a omorat.


### Diagrama UML

![Diagrama UML](/images/games/spaceshooter_uml.png)


