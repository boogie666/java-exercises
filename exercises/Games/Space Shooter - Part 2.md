# Space Shooter Part 2

In prima parte a acestu proiect am implementat baza jocului,
Entitatile...

In aceasta parte v-om defini un mod mai organizat de a creea, desena si update pentru entitati
numit `EntityManager`

Conform diagrame UML clasa `EntityManager` contine o serie de metode pentru a creea entitati
si o lista cu toate entitatile din joc.

Initial v-om creea clasa `EntityManager` intr-un mod mai simplu si v-om aduaga functionaliate
pe parcurs.

### O varinta simpla

Pentru inceput, v-om creea clasa `EntityManager` cu urmatoarele caracteristici:

  * o lista de entitati.
  * o metoda `update` care va itera prin lista si va apela metoda update pe fiecare entitate.
  * o metoda `render` care va itera prin lista si va apela metoda `render` pe fiecare entitate
  * o metoda `dispose` care va itera prin lista si va apela metoda `dispose` pe fiecare entitate
  * cate o metoda de `create` pentru fiecare tip de entitate din joc (3 momentan) care va aduga entitatea creata in lista



```java

public class EntityManager{
  private List<Entity> entities;
  
  public EntityManager(){
    this.entities = new ArrayList<Entity>();
  }
  
  public void createPlayer(){
    this.entities.add(new Player());
  }

  public void createBullet(float x, float y){
    this.entities.add(new Bullet(x, y));
  }

  public void createEnemy(float x){
    this.entities.add(new Enemy(x));
  }
    
  public void update(){
    for(int i = 0; i < this.entities.size(); i++){
      this.entities.get(i).update();
    }
  }
  public void render(SpriteBatch sb){
    for(int i = 0; i < this.entities.size(); i++){
      this.entities.get(i).render(sb);
    }
  }

  public void dispose(){
    for(int i = 0; i < this.entities.size(); i++){
      this.entities.get(i).dispose();
    }
    this.entities.clear(); // dupa ce a distrus toate entitatile, putem sa stregem tot ce ii in lista
  }
  
  public List<Entity> getEntities(){
    return this.entities;
  }
}

```

Dupa ce am creat clasa `EntityManager` o putem folosii in clasa `SpaceShooter`.

```java
public class SpaceShooter implements ApplicationListener {

	private SpriteBatch batch;
  private EntityManager manager;  

	@Override
	public void create() {
		this.batch = new SpriteBatch();
    this.manager = new EntityManager();
    this.manager.createPlayer();
    this.manager.createBullet(100, 100);
    this.manager.createEnemy(200);
	}

	@Override
	public void render() {
    this.manager.update();

		this.batch.begin();
    this.manager.render(this.batch);
		this.batch.end();
	}

	@Override
	public void dispose() {
		this.batch.dispose();
    this.mananger.dipose();
	}
}

```


Daca rulam jocul, observam nimic nu s-a schimbat. 
Jocul functioneaza exact la fel ca inaite de modificare, dar aceasta modificare ne permite
un grad mai mare de flexibilitate, in ceea ce priveste creea si managementul de entitati.


### The 'Shooter' bit din 'SpaceShooter'

Momentan observam ca in jocul nostru de tip `Shooter` nu putem sa
tragem...

Acum rectifica asta...

In clasa `Player`, cand jucatorul apasa pe `click stanga` va trebuii sa creeam un glont
la pozitia jucatorului.

Pentru asta clasa `Player` trebuie sa aibe access la o instanta de `EntityManager`
pe care sa apeleze, la un moment dat, metoda `createBullet`

pentru a putea face acest lucru ne uitam mai sus cu un nivel, in clasa `Entity`.

Putem observa, ca in diagrama UML, clasa `Entity` are un camp protected de tip `EntityManager`
si o metoda publica `setEntityManager` pentru a seta instanta entity manager...

La noi aceste doua lucruri nu exista, dar le putem adauga.


```
  you're on your own here... nu ii greu...
```

Dupa ce adaugam campul `entityManager` in clasa `Entity` si setter-ul pentru
el putem, la creea de entitati sa setam ce trebuie setat...


```
  public class EntityManager{
     ...
     public void createPlayer(){
        Player p = new Player();
        p.setEntityManager(this);
        this.entities.add(p);
     }
     // la fel si la createBullet si createEnemy
     ...
  }
```

Acum ca in avem access in clasa Player a un entityManager putem sa il folosim in metoda...

```
  public class Player{
     ...
     public void update(){
        this.x = Gdx.input.getX();
        this.y = Gdx.graphics.getHeight() - Gdx.input.getY();
        if(Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)){
           this.entityManager.createBullet(x, y);
        }
     }
     ...
  }

```

Acum daca rulam jocul, observa ca, nava jucatorului poate sa traga cu lasere.

### Chair daca merge, nu inseamna ca ii correct...

Codul nostru functioneaza...
Dar simplul fapt ca functioneaza, nu inseamna ca ii correct.


Daca urmarim flow-ul codului putem observa urmatoarul lucru...


  * EntityManager#update itereaza peste toate entitatile, inclusiv player-ul, si apleaza update.
  * metoda Player#update, creaza un bullet nou (apeland metoda createBullet)
  * metoda createBullet modifica lista de entitati din EntityManager.

Acest lucru ca codul nostru sa fie aibe posibile erori...
Erori care sunt greu de reprodus, si greu de depistat cand apar.

Dar, cu foarte mici modificari un clasa `EntityManager` putem garanta ca nu v-om avea erori niciodata.


In clasa `EntityManager` trebuie sa sincronizam adaugarea si stergea din lista entitati.

Putem creea doua  liste temporare, una pentru entitati care urmeaza sa fie adaugate si una pentru 
entitati care urmeaza sa fie sterse.

```
public class EntityManager{
  private List<Entity> entities;
  private List<Entity> toBeAdded;
  private List<Entity> toBeRemoved;
 
  public EntityManager(){
    this.entities = new ArrayList<Entity>();
    this.toBeAdded = new ArrayList<Entity>();
    this.toBeRemoved = new ArrayList<Entity>();
  }
  ... 
  public void createPlayer(){
    Player p = new Player();
    p.setEntityManager(this);
    this.toBeAdded.add(p); // aceeasi modificare trebuie facuta si la bullet si enemy
  }
  ...
  public void update(){
    this.entities.addAll(this.toBeAdded);
    this.toBeAdded.clear();

    for(int i = 0; i < this.entities.size(); i++){
      this.entities.get(i).update();
    }

    this.entities.removeAll(this.toBeRemoved);
    for(int i = 0; i < this.toBeRemoved.size(); i++){ // sa nu uitam sa distrugem entitatile cand le stergem din lista
      this.toBeRemoved.get(i).dispose();
    }
    this.toBeRemoved.clear();
  }
  ...

  // putem adauga si o metoda  in plus `remove`, ca v-om aveam  nevoie de ea mai tarziu
  public void remove(Entity e){
    this.toBeRemoved.add(e);
  }
}

```

Acum clasa EntityManager este corecta.
Nu mai exista nici o sansa de eroare in cod.

### Cleanup


Ar fii indicat sa stergem atat gloantele cat si inamicii la un moment dat.

In clasele `Bullet` si `Enemy` in metodele `update`,
cand entitatile aceastea ies din ecran, putem sa le stergem.

Pentru `Bullet` care iese din ecran in partea de sus putem sa facem urmtoarea

```
  public void update(){
    this.y += 20;
    if(this.y > 100) { // undeva dupa marginea de sus 
      this.entityManager.remove(this);
    }
  }
```

Iar pentru `Enemy` care iese din ecran in partea de jos

```
  public void update(){
    this.y -= 10;
    if(this.y < -100) { // undeva sub partea de jos
      this.entityManager.remove(this);
    }
  }

```


