# Space Shooter Part 3


In partea 2 am adaugat posibitatea de a trage cu lasere din nava.
In aceasta parte v-om adauga inamici si v-om pregatii terenul pentru 
a aduaga detectie de coliziune (intre gloante si inamici, si intre inamici si jucator)


### Inamici din spatiuuuuu


In jocul nostru, inamicii vor aparea la pozitii aleatoare din partea de sus a ecranului,
la un interval fix.

Pentru a face asta v-om modifica codul din clasa SpaceShooter.

va trebuii sa adaugam un camp de tip float numit `timer`, care va tine seama la cat timp a trecut din joc.
si la intervale fixe (de 0.5 secunde) bazat pe acest timer, v-om genera un inamic la o pozitie aleatoare.

Ulterior, dupa ce v-om avea posibilitatea de a trimie si procesa event-uri (folosind clasa EventBus, din UML)
v-om refactoriza acest cod, si il v-om muta intr-o clasa separata.


```java
public class SpaceShooter{
  
  ...  
  private float timer = 0;
  public void update(){
    timer += Gdx.graphics.getDeltaTime();
    if(timer >= 0.5f){
      timer = 0;  
      this.entityManager.createEnemy((float) (Math.random() * Gdx.graphics.getWidth()));
    }
    ...
  }

}

```

Daca rulam jocul acum, observam ca aproximative de doua ori pe secunda (sau odata la jumatate de secunda)
un inamic este creat in partea de sus a ecranului
Acest inamic nou creat, va merge de sus in jos, pana cand y-ul sau este mai mic decat -100, dupa care el va fi scos din joc.


### Pregatire pentru impact...


Din diagrama UML observam ca `Entity` are un camp de tipul `Rectangle` (se refera la clasa `com.badlogic.gdx.math.Rectangle`)
Aceasta clasa utilitara din libgdx modeleaza in cod notinea de dreptunghi si o serie de operatii matematice asociate 
unui drepunghi.

Cea mai important (pentru noi, in cazul de fata) este metoda `overlaps(Rectangle other);`
Aceasta metoda returneaza `true` daca dreptunghi-ul se suprapune cu alt dreptughi.

Folosind aceasta metoda putem determina daca doua entitati (care sunt practic drepunghiuri) se intersecteaza
(adica daca se lovesc)

Putem facem urmatoare modificare in clasa `Entity`

```
public abstract class Entity{
    ...
    protected Rectagle bounds;
    ...
    
    public boolean collidesWith(Entity other){
      return this.bounds.overlaps(other.bounds);
    }
    ...
}

```

Si in clasele Player, Bullet, si Enemy trebuie sa initializam 
obiectul `bounds`, cu acelasi dimensiuni ca textura.

Pe langa asta, in metoda `update`, dupa ce modificam coordonatele x/y,
care reprezinta pozitia unde se deseneaza entitatea, trebuie sa
modificam si pozitia dreptunghiului.


```
public class Player extends Entity {
  public Player(){
    ...
    this.texture = new Texture("player.png");
    // initializare 
    this.bounds = new Rectangle();
    this.bounds.setWidth(this.texture.getWidth());
    this.bounds.setHeight(this.texture.getHeight());
  }

  ...
  @Override
  public void update(){
    this.x = ...;
    this.y = ...;
    // dupa ce modificam pozitia entitati, trebuie sa modificam si pozitia 
    // dreptunghiul.
    this.bounds.setCenter(x,y);
    ...
  }
}
```

Pentru ca 'dimensiunea' entitati sa fie correcta, trebuie sa avem in vedere
ce dimensiune are poza folosita pentru entitate.

Daca poza este prea mare, drepunghiul in care se incadreaza entitatea v-a fi prea mare.

Ca solutie putem sa:
  1) modificam poza ca sa nu mai aibe margini in jurul content-ului
  2) sa definim manual width-ul si height-ul drepunghiului sa fie mai mic decat poza

Acum avem un mod de a verifica daca o entitate se 'loveste' de alta entitate.


In clasa Player (si ulterior in result entitatilor care dorim sa reactioneza la coliziune)
putem face urmatoarea modificare

```
public class Player{
  ...

  public void update(){
    ...

    List<Entity> allEntities = this.entityManager.getEntities();
    for(int i = 0; i < allEntities.size(); i++){
      Entity other = allEntities.get(i);
      if(other instanceof Enemy){ // daca entitatea pe care o verifica este de tipul Enemy
        if(this.collidesWith(other)){ // si daca ne lovim 
          System.out.println("Au!!!"); 
        }
      }
    }    
  } 
}
```

Daca rulam codul observam ca, de fiecare data cand ne intersectam cu un inamic,
consola va afisa mesajul "Au!!!" (de mai multe ori, pentru fiecare frame unde suntem in contact)

Acceasi logica se poate face si in classa Enemy, doar ca acolo trebuie sa verificam daca 
inamicul s-a intersectat cu un Bullet.
