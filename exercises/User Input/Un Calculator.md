# Un Calculator

Scrieti un program care permite utilizatorlui sa introduca doua numere de la tastatura 
si afiseaza suma celor doua numere


### Pseudocod
```
fie s = un scanner nou.

afiseaza "Introduceti primul numar:"
fie primulNumar = s.nextDouble();

afiseaza "Introduceti al doilea numar:"
fie alDoileaNumar = s.nextDouble();

afiseaza "Suma este " + (primulNumar + alDoileaNumar);
```
