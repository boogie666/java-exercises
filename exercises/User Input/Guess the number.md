# Guess the number.


Scrieti un 'joc' care permite user-ul sa ghiceasca un numar intre 0 si 10.

User-ul va introduce de la tastatura numarul.

Daca user-ul a ghicit correct programul v-a afisa "Bine Bo$$ ai ghicit!"
Altfel va afisa "Bo$$ ce faci... nu-i bine"


### Pseudocod

```
fie s = un scanner nou,
    numarDeGhicit = Math.floor(Math.random() * 10);

afiseaza "Ma gandesc la un numar intre 0 si 10. Ghici la cat?"
fie rezultatDeLaUser = s.nextInt();

daca numarDeGhicit este egal cu rezultatDeLaUser:
  afiseaza "Bine Bo$$ ai ghicit!"
altfel:
  afiseaza "Bo$$ ce faci... nu-i bine"

```
