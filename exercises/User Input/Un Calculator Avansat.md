# Un Calculator Avansat

Scrieti un program care permite utilizatorlui sa introduca doua numere de la tastatura 
si un din urmatoarele simboluri + - / *
si afiseaza calculul aferent simbolului.


### Pseudocod
```

fie s = un scanner nou.

afiseaza "Introduceti operatia:"
fie operatia = s.next(); // s.next retunreaza urmatorul sir de charactere introdus

afiseaza "Introduceti primul numar:"
fie primulNumar = s.nextDouble();

afiseaza "Introduceti al doilea numar:"
fie alDoileaNumar = s.nextDouble();

daca operatia.equals("+"):
  afiseaza primulNumar + "+" + alDoileaNumar + " = " + (primulNumar + alDoileaNumar)
altfel daca operatia.equals("-"):
  afiseaza primulNumar + "-" + alDoileaNumar + " = " + (primulNumar - alDoileaNumar)
altfel daca operatia.equals("/"):
  afiseaza primulNumar + "/" + alDoileaNumar + " = " + (primulNumar / alDoileaNumar)
altfel daca operatia.equals("*"):
  afiseaza primulNumar + "*" + alDoileaNumar + " = " + (primulNumar * alDoileaNumar)
altfel:
  afiseaza "Operatia " + operatia + " nu este permisa"

```
