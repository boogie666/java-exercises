# git


Git este un system de control al versiunilor folosit in majoritatea proiectelor software
(fie ele proiecte java sau altceva)

Git a fost creat de celebrul Linus Torvalds (creator-ul Linux-ului) pentru a manage-ui codul sursa al proiectul Linux...

Practic `git` poate fi vazut ca un `undo` universal care iti permite sa revii oricand la o versiune anterioara a codului.
Altfel spus, `git` face un `backup` la fisiere si iti permite sa revi la acele fisiere oricand doresti.



### Integrare cu IDE-uri

`git` ca tool de sine statator sau ca plugin integrat in majoritatea IDE-urilor.

Pentru eclipse gasiti un tutorial [aici](https://www.vogella.com/tutorials/EclipseGit/article.html) 
Pentru intellij gasiti [aici](https://www.logicbig.com/tutorials/misc/git/intellij.html)


### Comunitate

Git este folosit foarte des in proiectele open-source.
Comunitatea globala de developer colaboreaza foarte des pentru a creea proiecte interesante impreuna.

Cel mai folosit site pentru asta este [github.com](https://www.github.com)
(o alternativa buna este [gitlab.com](https://www.gitlab.com))

Pe aceste site-uri gasiti multe proiect si biblioteci foarte utile printre care si [libGDX](https://github.com/badlogic/libgdx) :)


