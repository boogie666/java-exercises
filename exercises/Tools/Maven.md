# Maven


Maven este un tool folosit in principal pentru managementul dependintelor unui proiect.
Folosind diverse plugin-uri Maven poate face mult mai multe.
Cum ar fi compilare si packaging-ul unui proiect, generarea de documentatie, rularea testelor automate si multe altele.


### pom.xml

Maven este configurat printr-ul fisier numit `pom.xml`
Dupa cum putem deduce din extensia fisierlui, este in formatul `xml`
Fisierul `pom.xml` descrie proiectul nostru.

Acest format arata asa:

```
<project>
  <modelVersion>4.0.0</modelVersion>

	<groupId>ro.itschool</groupId>
	<artifactId>space-shooter</artifactId>
	<version>0.0.1</version>
	<name>Space Shooter</name>
	<description>Un joc in SPATIUUUU!!!!</description>

	<dependencies>
		<dependency>
			<groupId>com.badlogicgames.gdx</groupId>
			<artifactId>gdx</artifactId>
			<version>1.9.10</version>
		</dependency>
	</dependencies>

</project>

```

Observam in codul de mai sus o serie de `tag`-uri.
fiecare din ele avand ca si copil ori un alt tag, ori o valoare.

De exemplu: 
tag-ul `project` are copii `modelVersion` `groupId` `artifactId` `version` `name` `description` si `dependencies`.
tag-ul `name` are valoare `Space Shooter`
tag-ul `dependencies` are un alt tag ca si copil (si anume una din bibliotecile de care depinde proiectului).


Putem observa ca proiectul nostru se cheama `Space Shooter` (tag-ul `name`) 
are id-ul `space-shooter` (tag-ul `artifactId`)
este creat de grupul `ro.itschool` (tag-ul `groupId`)
are versiunea `0.0.1` (tag-ul `version`)

si depinde de biblioteca `gdx` creata de grupul `com.badlogicgames.gdx` cu versiunea `1.9.10`

Biblioteca `gdx` are la randul ei un `pom.xml` unde si ea isi specifica o serie de informatii (printre care si dependintele ei)


### Usecase pentru Maven...

Daca ar fii sa folosim manual biblioteca `gdx` (sau orice alta biblioteca) ar trebuii sa includem manual (pe langa biblioteca `libgdx`) toate
dependintele ei, dar si toate dependintele dependintelor ei etc...

Folosind Maven nu este nevoie nici macar sa stim ca biblioteca pe care dorim sa o folosim are dependinte, pentru ca Maven se va ocupa 
sa descarce de pe internet tot ce ii necesar pentru ca proiectul nostrul (evident doar partile descrise in `pom.xml`) sa ruleze cum trebuie.


### Mod de folosire

Eclipse (si majoritatea IDE-urilor de java) au integrare foarte buna cu maven fara nici un fel de configrare extra.
Deci tot ce trebuie sa facem este sa creem un fisier numit `pom.xml` la baza proiectului si sa punem in el datale relevante
(evident in formatul corect)


Puteti gasi mai multe explicatii [aici](https://www.youtube.com/watch?v=KNGQ9JBQWhQ), [aici](https://www.youtube.com/results?search_query=java+maven+tutorial)
sau [aici](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)

