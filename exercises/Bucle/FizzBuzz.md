
# Fizz Buzz!

Afisati primele 100 de numere naturale cu urmatoarele conditii:
  * Cand numarul este divizibil cu 3 afisati Fizz
  * Cand numarul este divizibil cu 5 afisati Buzz
  * Cand numarul este divizibil cu 3 si 5 afisati FizzBuzz
  * Altfel afisati numarul.

Spunem ca un numar 'x' este divizibil cu alt numar 'y' daca 
restul impartirii lui x la y este 0 

adica `x % y == 0`

Ex:

1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,FizzBuzz,16


### Pseudocod

```
  pentru fiecare i de la 0 la 100:
    daca i este divizibil cu 3 si i este divizibil cu 5:
      afiseaza "FizzBuzz";
    altfel daca i este divizibil cu 3:
      afiseaza "Fizz";
    altfel daca i este divizibul cu 5:
      afiseaza "Buzz";
    altfel:
      afiseaza i;
```
