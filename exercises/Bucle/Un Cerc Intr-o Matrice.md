# Matrice cu un cerc in mijloc

Creati o matrice patratica NxN care sa aibe un cerc umplut cu '.' si inconjurat de 'x'

Un punct (i,j) este in cercul C daca distanta intre centrul cercului C si punctul (i,j) este
mai mica decat raza cercului.

Astfel putem folosi ecuatia urmatoare pentru a determina daca un punct (i,j) este in/pe cerc sau nu.
unde 'a' si 'b' sunt centrul cercului si 'r' este raza lui.

```
    (i - a)*(i - a) + (j - b)*(j - b) <= r * r
```

Pro Tip:

1.  Folositi variablie ajutatoare in implementare pentru a face codul mai clar.
2.  Pentru a pune cercul pe centrul matricii:
    *   raza cercului este N/2, unde N este este dimensiunea matrici
    *   centrul cercului este a = (N-1)/2 si b = (N-1)/2
3.  dimensiunea matricii determina 'rezolutia' rezultatului, cu cat ii mai mare N cu atat mai mult o sa arate ca un cerc, dupa implementare incercati alte valori mai mari pentru N

Pentru o matrice patratica NxN unde N=10 rezultatul este:

```
    xxx....xxx
    x........x
    x........x
    ..........
    ..........
    ..........
    ..........
    x........x
    x........x
    xxx....xxx
```

### Pseudocod
```
    fie N = 10,
        a = (N - 1) / 2,
        b = (N - 1) / 2,
        r = N / 2;
    
    pentru fiecare i de la 0 la N:
      pentru fiecare j de la 0 la N:
        fie distantaDeLaCentru = (i - a) * (i - a) + (j - b) * (j - b),
            estePunctulInCerc =  distantaDeLaCentru <= r * r;

        daca estePunctulInCerc:
          afiseaza '.'
        altfel
          afiseaza 'x'
```
