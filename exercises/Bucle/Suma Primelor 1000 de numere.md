# Suma a N numere naturale

Calculati suma a primelor 1000 de numere naturale (de la 0 999)

### Pseudocod

```
    fie  n = 1000,
         resultat = 0;

    pentru fiecare i, de la 0 la n:
      resultat = resultat + i;

    afiseaza resultat;
```


