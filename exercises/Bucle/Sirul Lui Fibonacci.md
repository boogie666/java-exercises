# Fibonacci

Afisati al 25-lea numar din sirul lui fibonacci.

Sirul lui fibonacii este definit in felul urmator:

```
    fibonacci[n] = fibonacci[n-1] + fiboancci[n-2]
    fibonacci[1] = 1
```

primele zece numere din sir sunt:

1 1 2 3 5 8 13 21 34 55

### Pseudocod
```
    fie n=25, 
        a=1,
        b=1;

    cat timp n > 2: // n > 2 pentru ca 1,1 sunt primele doua valori din sir
      fie tmp = b;
      b = a + b;
      a = tmp;
      n = n - 1;

    afiseaza b;
```
