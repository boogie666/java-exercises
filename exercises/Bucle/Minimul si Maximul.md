# Determinati minimul si maximul dintr-un array.

Creati un array 'numere' de 10 element de tip 'numar natural'  
cu numerele: 1,5,7,10,11,-2,-5,0,9 si 3

Determinati simultan minimul si maximul din 'numere' si afisati  
mesajul 'Maximul este ' + max si 'Minimul este ' + min.


### Pseudocod

```
  fie N = [1,5,7,10,11,-2,-5,0,9, 3],
      max = N[0],
      min = N[0];

  pentru fiecare i de la 1 la lungimea lui N:
    daca max < N[i]:
      max = N[i];
    daca min > N[i]:
      min = N[i];


  afiseaza 'Maximul este' + max;
  afiseaza 'Minimul este' + min;

```
