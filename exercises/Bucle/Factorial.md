# Factorial!

Calculati 10! (citit '10 factorial')

Factorial este definit in felul urmator:
```
    N! = 1 * 2 * 3 * ... * N
```

### Pseudocod

```
    fie n = 10,
        resultat = 1;

    pentru fiecare i, de la 1 la n - 1:
       resultat = resultat * i;

    afiseaza rezultat;   
```


