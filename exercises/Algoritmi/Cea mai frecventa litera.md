# Cea mai frecventa litera

Afisati caracterele din propozitia "Ana are mere!"
care apar mai mult de odata.


Pentru a rezoval problema putem observa urmatoarele caracteristici:

  * In java, un character are corespondent numeric direct, 'a' este 97, 'b' este 98 etc...
  * Un vector (de exemplu v=[10,2,0,1]) poate fi vazut ca o mapare de la index la valoare, astfel:
       * 0 se mapeaza la 10 (pentru ca v[0] == 10)
       * 1 se mapeaza la 2  (pentru ca v[1] == 2)
       * 2 se mapeaza la 0  (pentru ca v[2] == 0)
       * 3 se mapeaza la 1  (pentru ca v[3] == 1)


Putem creea un vector care reprezinta maparea dintre un character si de cate ori apare in text.
Pentru asta vom avea nevoi de un vector de 127 de pozitii, pentru ca sunt 127 de caracter in tabla ASCII, tabla de corespondenta intre caractere si numere) 

![tabela ascii](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/ASCII-Table-wide.svg/875px-ASCII-Table-wide.svg.png)

Stim ca in java avand `char c = 'a';`
Putem sa il transformam intr-un 'int' in felul urmator `int i = (int) c;`

Si daca avem avem un `int i = '97';`
Putem sa il transformam in 'char' asa `char c = (char) i;`

Mai stim ca un String poate fi convertit la un char[] 
folosind metoda toCharArray.
String "Ana are mere!" poate fi reprezentat intr-un vector de caractere asa:
```java
  char[] propozitie = "Ana are mere!".toCharArray();
```


Astfel algoritmul nostru va arata in felul urmator:

Iteram prin sirul de caractere dat.
pentru fiecare litera (stiind ca o litera este defapt un numar) incrementam numarul de la 
pozitia corespunzatoare din tabela noastra de mapare.

La final, iteram prin tabela de mapare, si afisam doar literele cu valoare mai mare sau egala cu 2.

### Pseudocod

```
  fie propozitie = "Ana are mere!",
      tabelaDeMapare = [127 de pozitii de tip numar intreg];


  pentru fiecare c, c fiind o litera din propozitie:
    tabelaDeMapare[c] = tabelaDeMapare[c] + 1;

  
  pentru fiecare i de la 0 la lunginea lui tabelaDeMapare:
    daca tabelaDeMapare[i] este mai mare sau egal cu 2:
      afiseaza i transformandul in 'char'

```
