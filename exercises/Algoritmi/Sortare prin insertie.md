# Sortare prin insertie.

Sortati vectorul [6,5,3,1,8,7,2,4] in ordine crescatoare.

O posibila solutie la aceasta problema este sa folosim algoritmul 
Insertion Sort.

Acest algoritm presupune sa gasim cel mai mic element din vector
si sa il plasam pe prima pozitie.

Restul elementelor se muta cu o pozitie mai in spate.

Acesta procedura se repeta pana cand am parcurs tot vectorul.

In felul urmator:

![insertion sort](https://upload.wikimedia.org/wikipedia/commons/0/0f/Insertion-sort-example-300px.gif)


[Gasiti mai multe detalii aici](https://en.wikipedia.org/wiki/Insertion_sort)



### Pseudocod

```
    fie A = [6,5,3,1,8,7,2,4],
        i = 1;

    cat timp i < lungimea lui A:
      fie x = A[i],
          j = i - 1;
      cat timp j >= 0 si A[j] > x:
        A[j+1] = A[j];
        j = j - 1;

      A[j + 1] = x;
      i = i + 1;


    afiseaza A; // atentie aici... A este array, System.out.println(A) nu face ce trebuie.
```
