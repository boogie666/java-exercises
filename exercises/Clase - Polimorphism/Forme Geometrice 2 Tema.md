# Forme Geometrice #2 - Fancy Refacotring

Modificati formele geometrice de create anterior astfel in cat
fiecare forma sa se deseneze cu centru la 0,0

Implementati 2 noi transformari asupra unei forme:

Translatare si Rotire.


#### Translatare

Translatarea se implementeaza scazand din coordonatele i,j primite in continePunct cu alte valori...

```
  noulI = i - offsetX
  noulJ = j - offsetY
```

de exemplu daca vrem sa mutam un cerc codul ar putea sa arate asa:
```
  Forma cerc = new Translatere(5, 5, new Cerc(4)); // offsetX = 5, offsetY = 5
  Forma cerc2 = new Translatere(15, 6, new Cerc(4)); // offsetX = 15, offsetY = 6
```

#### Rotire

Rotirea se face conform urmatoarei formule:

```
  noulI = i * cos(unghi) - j * sin(unghi);
  noulJ = i * sin(unghi) + j * cos(unghi);
```
iar unghiul trebuie specificat in radiani.

Pentru a convertii un unghiul din grade in radiani putem folosii urmatoarea formula:

```
  unghi = unghiInGrade * PI / 180;
```

Pentru un patrat rotit la 45 de grade codul ar arata asa:

```
  Forma patrat = new Rotire(45, new Dreptunghi(5,5));
  
```

#### Obervatie

  1) Transformarile implementate se numesc [Transformari Afine](https://en.wikipedia.org/wiki/Affine_transformation)
  2) Ordinea aplicarii conteaza, `Rotire(Translatere(forma))` nu va produce acelasi rezultat cu `Translatare(Rotire(forma))`
  3) Putem observa ca clasele de Uniune, Intersectie, Inversare si Scena raman nemodificate 
in urma aceste refatorizari. Ele se pot folosii incontinuare in exact aceeasi mod ca pana acum...


### Pseudocod
```
  // cerc cu centru la 0, 0
  clasa Cerc(raza) extinde Forma:
    continePunct(i, j):
      return i * i + j * j <= raza * raza;
  
  // dreptunghi cu centrul la 0, 0
  clasa Dreptunghi(lungime, inaltime) extinde Forma:
    continePunct(i, j):
      retunreaza subLiniaDeSus(i,j) si deasupraDeLiniaDeJos(i, j) si 
                 laStangaDeLiniaDinDreapta(i, j) si laDreaptaDeLiniaDinStanga(i, j);

    subLiniaDeSus(i, j):
      retuneaza i <= -lungime / 2;
    
    deasupraDeLiniaDeJos(i,j):
      retuneaza i >= lungime / 2;

    laStangaDeLiniaDinDreapta(i, j):
      retunreaza j >= - inaltime / 2;
    
    laDreaptaDeLiniaDinStanga(i, j):
      retunreaza j <= inaltime / 2;


  // clasele de tema
  clasa Translatare extinde Forma:
    Forma f;
    int offsetX;
    int offsetY
    constructor(oX, oY, forma):
      f = forma;
      offsetX = oX;
      offsetY = oY;

    continePunct(i, j):
      retruneaze // aici faceti voi


  clasa Rotire extinde Forma:
    Forma f;
    float unghi;
    constructor(unghiInGrade, forma):
      f = forma;
      unghi = // aici formula de grade in radiani

    continePunct(i, j):
      returneaza // aici faceti voi
    
```
