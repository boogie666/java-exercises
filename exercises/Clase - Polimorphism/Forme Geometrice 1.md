# Forme Geometrice #1 - Polymorphism

Creati o matrice patratica NxN care sa aibe mai multe forme desenate in ea.
Matricea va contine '#' unde e spatiu gol si '.' unde se afla formele.

Scena va desena o goagoasa.

#### Polimorphism 


Putem observa din cod ca Scena are nevoie doar de metoda `continePunct(i,j)`
pentru a putea desena ceva.

Pentru a implementat mai abstract biblioteca noastra de desenat
va trebui sa folosim notiunea de polimorphism si o clasa abstract
generica, numatia Forma.

Toate formele noastre vor fi derivate din clasa de baza Forma 
si vor mosteni metoda abstract `continePunct(i, j)`

Acest lucru ne permite sa generalizam clasa de desenare (numata Scena)

Astfel, clasa Scena, nu v-a mai stii de Cercuri, sau Dreptughiuri, 
sau orice alta forma concreta, 
Scena v-a stii doar de conceptul abstract de `Forma`.

Din cauza aceste generalizari, Scena a pierdut posibilitatea de a desena mai multe obiecte.
Dar folosind polimorphism si un pic de creativitate, putem redobandii aceasa posibilitate.

Observam ca clasa abstracta Forma nu stie nimic de cum sa se deseneze...
Mai observam ca Forma nu stie ce forma ii...

Putem creea un nou fel "forma" care nu reprezinta o forma gemeotrica ci,
uniunea dintre doua forme geometrice.

```
  clasa Uniune extinde Forma:
    Forma a;
    Forma b:
    constructor(formaA, formaB):
      a = formaA;
      b = formaB;
  
    @Override
    continePunct(i, j):
      returneaza a.continePunct(i, j) SAU b.continePunct(i, j);

```

Similar putem creea inca o "forma", care sa reprezinte
Intersectia a doua forme...


```
  clasa Intersectie extinde Forma:
    Forma a;
    Forma b;
    constructor(formaA, formaB):
      a = formaA;
      b = formaB;
    
    @Override
    continePunct(i, j)
      returneaza a.continePunct(i, j) SI b.continePunct(i, j);

```

### Pseudocod
```

    clasa abstracta Forma:
      metoda abstracta continePunct(i, j) care retunreaza un boolean

    clasa Cerc(x, y, raza) extinde Forma:
      @Override
      continePunct(i, j):
        retruneaza // vezi implementare la 'Class/Forme Geometrice 1.md

    clasa Dreptunghi(x, y, lungime, latime) extinde Forma:
      @Override
      continePunct(i, j):
        retruneaza // vezi implementare la 'Class/Forme Geometrice 2.md

     
    
    clasa Uniune extinde Forma:
      Forma a;
      Forma b:
      constructor(formaA, formaB):
        a = formaA;
        b = formaB;
    
      @Override
      continePunct(i, j):
        returneaza a.continePunct(i, j) SAU b.continePunct(i, j);
  
    
    clasa Intersectie extinde Forma:
      Forma a;
      Forma b;
      constructor(formaA, formaB):
        a = formaA;
        b = formaB;
      
      @Override
      continePunct(i, j)
        returneaza a.continePunct(i, j) SI b.continePunct(i, j);
     
    clasa Invers extinde Forma // pe asta o faceti voi
    // clasa va accepta o singura forma prin constructor si 
    // in metoda continePunct, va returna opusul rezultatului formei primite.

    clasa Scena:
      Forma forma;
      int laturaMatrice;
      contructor(N, f):
          laturaMatrice = N;
          forma = f;
      
      deseneaza():
        pentru fiecare y de la 0 la laturaMatrice:
          pentru fiecare x de la 0 la laturaMatrice:
             daca forma.continePunct(x, y):
                afiseaza '.'
             altfel:
                afiseaza '#'


      fie functia main:
         Cerc c1 = creaza un Cerc nou cu 10, 10, 5;
         Cerc c2 = creaza un Cerc nou cu 10, 10, 3;
         Invers i = creaze un Invers nou din c2;
         Intersectie gogoasa = creaza o noua Intersectie intre c1 si i;
          
         Scena s = creaza o Scena noua cu 20 si gogoasa;
         s.deseneaza();
```
