# Creaza o functie care care afiseaza un vector


Afiseaza urmatrii 4 vectori:
 
  * [1,4,3,2,5]
  * [7,8,9,10,3]
  * [1,2,3,4,5,6]
  * [1,2,3]



In format-ul urmator:


  "[" + numerele din vector separate cu spatiu + "]";


### Pseudocod

```

fie afiseazaVector([vector]) care nu retureaza nimic:
  afiseaza "[" fara linie noua;
  pentru fiecare i de 0 la lungimea lui vector:
    afiseaza vector[i] fara linie noua;
    afiseaza spatiu;
  
  afiseaza "]" cu linie noua;

 
afiseazaMatrice([1,4,3,2,5]);
afiseazaMatrice([7,8,9,10,3]);
etc...

```
