# Verifica diagonala unei matrici

Creaza o functie care verifica daca toate elemente de pe diagonala principala a unei matrici de 3x3 contine cifra 1:

de exemplu:
Pentru matricea urmatoare, functia va returna `true`
```
100
010
001
```

Pentru matricea urmatoare functia va returna `false`

```
100
000
001
```

Pentru a putem resolva problema in doua moduri.

1) putem observa ca matricea este de 3x3 tot timpu, astfel rezolvand problema specific pentru o matrice de 3x3
2) putem rezolva generic problema, astfel rezolvand problema pentru pentru orice matrice patratica.


### Pseudocod 1

Solutie specifica pentru o matrice de 3x3:

```
  fie verificaDiagonala([[matrice de numere intregi]]) o functie care retunreaza un boolean:
    daca matrice[0][0] este 1 si matrice[1][1] este 1 si matrice[2][2] este 1:
      returneaza 'adevarat'
    altfel:
      returneaza 'fals'
```


### Pseudocod 2

Solutie generice pentru orice matrice patratica:

Pentru cazul acesta, plecam de la premiza ca matricea contine 1 pe diagonala principala
si incercam sa demonstram contrariul.

Practic, cand gasim prima valoarea de pe diagonala care nu este correcta
returnam 'fals'. daca nu am gasit nici o situatie incorecta, putem trage concluzie ca
pe diagonala principala avem doar cifra 1, si returnam 'adevarat'

```
  fie verificaDiagonala([[matrice de numere intregi]]) o functie care retunreaza un boolean:
    pentru fiecare i de la 0 pana la lungimea lui matrice:
      pentru fiecare j de la 0 pana la lungimea lui matrice[i]:
        daca i este egal j si daca matrice[i][j] este diferit de 1:
          returneaza 'fals';

    returneaza 'adevarat';
```
