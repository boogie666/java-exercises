# Suma cifrelor primelor 100 de numere naturale

Sa se calculeze suma cifrelor primelor 100 de numere naturale.

### Hint-uri

Stim sa calculam suma cifrelor unui singur numar.
Algoritmul este simular cu cel pentru a determina daca un numar este palindrom.
Si simular cel care face suma numerelor dintr-un vector.


Stim sa determinal primele 100 de numere naturale.
Este o bucla de la 1 la 100 (inclusiv)


Tot ce ramane este sa le punem cap la cap.


### Pseudo-Pseudocod

```
  generam primele 100 de numere naturale.
  pentru fiecare numar adunam la un total sumaCifre(numar);
  afiseaza total;
```
