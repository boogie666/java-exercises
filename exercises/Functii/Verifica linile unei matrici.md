# Verifica linile unei matrici

Creaza o functie care verifica daca toate elemente de pe o linie a unei matrici de 3x3 contine cifra 1:

de exemplu:
functia va returna `true` pentru:

```
111      000      000     010
000 sau  111 sau  000 sau 111 etc.
000      000      111     100
```

Pentru orice alte matrice care nu au pe cel putin o linie cu cifra 1, functia va retrunea `false`

Apelati functie pe 3 exemple de matrice care retruneaza `true` si 3 exemple pentru care returneaza `false`

### Pseudocod

Obvservam in cazul acesta ca nu avem nevoie de doua bucle.
`i` este in cazul acesta dat ca parametru (cu numele `linie`)

```
  fie verificaLinie([[matrice]], linie de tip intreg) o functie care retruneaza un boolean:
    pentru fiecare j de la 0 la lungimea lui matrice:
      daca matrice[linie][j] este diferit de 1:
        returneaza 'fals'
    
    returneaza 'adevarat'
```



