# Media Vecinilor

Creati o functie care accepta un vector de numere reale ca paramentru si returneaza alt vector de numere reale.

Fiecare element din rezultat calculat facand media aritmetica intre el si elementele vecine.


De exemplu:

Pentru vectorul de intrare V=[1,2,3] rezutatul este [1.5,3,2.5]

adica:
```
  V[1] = (V[1] + V[2]) / 2
  V[2] = (V[1] + V[2] + V[3]) / 3
  V[3] = (V[2] + V[3]) / 2
```


Altfel spus.

Fiecare element din vectorul rezultat este calculat dupa formula:

v[i] = (v[i - 1] + v[i] + v[i+1]) / 3

Atentie:

pentru i = 0 si i = lungimea lui V - 1 exista numa doi vecini.


### Pseudocod

```
  fie mediaVecinilor([vector de numere reale]) o functie care returneaza un vector de numere reale:
    fie rezultat = [un vector de numere reale de aceasi lungime ca 'vector'];
    pentru fiecare i de 0 la lungimea lui vector:
      daca i este egal cu 0:
        rezultat[i] = (vector[i] + vector[i + 1]) / 2;
      altfel daca i este egal cu lungimea lui vector - 1:
        rezultat[i] = (vector[i - 1] + vector[i]) / 2;
      altfel:
        rezultat[i] = (vector[i - 1] + vector[i] + vector[i + 1]) / 2;

    returneaza rezultat;
```


