# Calculati cat de des apare fiecare cifra.


Scrieti o functie care determina cat de des apare fiecare cifra dintr-un numar.

Afisati rezultatele pentru urmatoarele numere:
 
12344, 1337, 156656, 5431


Adaca o cifra apare de 0 ori, nu afisati rezultat pentru cifra aceea.


### Pseudo-Pseudocod

```
  fie frecventaCifre(numar) o functie care returneaza un vector de 10 element (cifrele de la 0 la 9):
    determinam frecventele similar cu problema 'Cea mai frecventa litera'
    returnam vectorul de frecvente;


  // atentie, frecventaCifre returneaza un vector si trebuie afisat altfel...
  afiseaza frecventaCifre(12344);
  afiseaza frecventaCifre(1337);
  afiseaza frecventaCifre(156656);
  afiseaza frecventaCifre(5431);
```
