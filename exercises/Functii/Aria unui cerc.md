# Creaza o functie care a calculeaza aria unui cerc


Aria unui cerc este definita ca 2 * PI * raza^2 (raza la patrat).

Constanta PI, in java, se gaseste in clasa Math si se poate accesa in felul urmator:
`Math.PI`

Folosind functia aceasta calculati aria unui cerc de raza 7.43


### Pseudocod

```
fie arieCerc(raza) o functie care returneaza un double:
  returneaza 2 * Math.PI * raza * raza;

afiseaza arieCerc(7.43);
```
